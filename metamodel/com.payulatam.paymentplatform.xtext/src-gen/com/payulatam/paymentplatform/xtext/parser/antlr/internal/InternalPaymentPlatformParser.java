package com.payulatam.paymentplatform.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.payulatam.paymentplatform.xtext.services.PaymentPlatformGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPaymentPlatformParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_COLON", "RULE_SQUARE_LEFT_BRACKET", "RULE_COMMA", "RULE_SQUARE_RIGHT_BRACKET", "RULE_STRING", "RULE_ID", "RULE_LEFT_BRACKET", "RULE_RIGHT_BRACKET", "RULE_ARROW", "RULE_EQUAL", "RULE_SEMICOLON", "RULE_LEFT_PARENTHESIS", "RULE_RIGHT_PARENTHESIS", "RULE_DOT", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'In'", "'the'", "'adapter'", "'payment'", "'network'", "'uses'", "'check'", "'in'", "'url'", "'branch'", "'RestPaymentNetwork'", "'SOAPPaymentNetwork'"
    };
    public static final int RULE_SQUARE_LEFT_BRACKET=5;
    public static final int RULE_STRING=8;
    public static final int RULE_SL_COMMENT=20;
    public static final int RULE_RIGHT_BRACKET=11;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int RULE_ARROW=12;
    public static final int RULE_LEFT_PARENTHESIS=15;
    public static final int RULE_DOT=17;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_SQUARE_RIGHT_BRACKET=7;
    public static final int T__32=32;
    public static final int RULE_RIGHT_PARENTHESIS=16;
    public static final int RULE_ID=9;
    public static final int RULE_EQUAL=13;
    public static final int RULE_COMMA=6;
    public static final int RULE_WS=21;
    public static final int RULE_COLON=4;
    public static final int RULE_LEFT_BRACKET=10;
    public static final int RULE_ANY_OTHER=22;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=18;
    public static final int T__29=29;
    public static final int RULE_ML_COMMENT=19;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_SEMICOLON=14;

    // delegates
    // delegators


        public InternalPaymentPlatformParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPaymentPlatformParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPaymentPlatformParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPaymentPlatform.g"; }



     	private PaymentPlatformGrammarAccess grammarAccess;

        public InternalPaymentPlatformParser(TokenStream input, PaymentPlatformGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "PaymentPlaform";
       	}

       	@Override
       	protected PaymentPlatformGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRulePaymentPlaform"
    // InternalPaymentPlatform.g:64:1: entryRulePaymentPlaform returns [EObject current=null] : iv_rulePaymentPlaform= rulePaymentPlaform EOF ;
    public final EObject entryRulePaymentPlaform() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePaymentPlaform = null;


        try {
            // InternalPaymentPlatform.g:64:55: (iv_rulePaymentPlaform= rulePaymentPlaform EOF )
            // InternalPaymentPlatform.g:65:2: iv_rulePaymentPlaform= rulePaymentPlaform EOF
            {
             newCompositeNode(grammarAccess.getPaymentPlaformRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePaymentPlaform=rulePaymentPlaform();

            state._fsp--;

             current =iv_rulePaymentPlaform; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePaymentPlaform"


    // $ANTLR start "rulePaymentPlaform"
    // InternalPaymentPlatform.g:71:1: rulePaymentPlaform returns [EObject current=null] : ( () otherlv_1= 'In' ( (lv_name_2_0= ruleEString ) ) this_COLON_3= RULE_COLON otherlv_4= 'the' otherlv_5= 'adapter' this_SQUARE_LEFT_BRACKET_6= RULE_SQUARE_LEFT_BRACKET ( (lv_adapter_7_0= ruleAdapter ) ) (this_COMMA_8= RULE_COMMA ( (lv_adapter_9_0= ruleAdapter ) ) )* this_SQUARE_RIGHT_BRACKET_10= RULE_SQUARE_RIGHT_BRACKET otherlv_11= 'the' otherlv_12= 'payment' otherlv_13= 'network' this_SQUARE_LEFT_BRACKET_14= RULE_SQUARE_LEFT_BRACKET ( (lv_paymentnetwork_15_0= rulePaymentNetwork ) ) (this_COMMA_16= RULE_COMMA ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) ) )* this_SQUARE_RIGHT_BRACKET_18= RULE_SQUARE_RIGHT_BRACKET ) ;
    public final EObject rulePaymentPlaform() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token this_COLON_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token this_SQUARE_LEFT_BRACKET_6=null;
        Token this_COMMA_8=null;
        Token this_SQUARE_RIGHT_BRACKET_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token this_SQUARE_LEFT_BRACKET_14=null;
        Token this_COMMA_16=null;
        Token this_SQUARE_RIGHT_BRACKET_18=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_adapter_7_0 = null;

        EObject lv_adapter_9_0 = null;

        EObject lv_paymentnetwork_15_0 = null;

        EObject lv_paymentnetwork_17_0 = null;



        	enterRule();

        try {
            // InternalPaymentPlatform.g:77:2: ( ( () otherlv_1= 'In' ( (lv_name_2_0= ruleEString ) ) this_COLON_3= RULE_COLON otherlv_4= 'the' otherlv_5= 'adapter' this_SQUARE_LEFT_BRACKET_6= RULE_SQUARE_LEFT_BRACKET ( (lv_adapter_7_0= ruleAdapter ) ) (this_COMMA_8= RULE_COMMA ( (lv_adapter_9_0= ruleAdapter ) ) )* this_SQUARE_RIGHT_BRACKET_10= RULE_SQUARE_RIGHT_BRACKET otherlv_11= 'the' otherlv_12= 'payment' otherlv_13= 'network' this_SQUARE_LEFT_BRACKET_14= RULE_SQUARE_LEFT_BRACKET ( (lv_paymentnetwork_15_0= rulePaymentNetwork ) ) (this_COMMA_16= RULE_COMMA ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) ) )* this_SQUARE_RIGHT_BRACKET_18= RULE_SQUARE_RIGHT_BRACKET ) )
            // InternalPaymentPlatform.g:78:2: ( () otherlv_1= 'In' ( (lv_name_2_0= ruleEString ) ) this_COLON_3= RULE_COLON otherlv_4= 'the' otherlv_5= 'adapter' this_SQUARE_LEFT_BRACKET_6= RULE_SQUARE_LEFT_BRACKET ( (lv_adapter_7_0= ruleAdapter ) ) (this_COMMA_8= RULE_COMMA ( (lv_adapter_9_0= ruleAdapter ) ) )* this_SQUARE_RIGHT_BRACKET_10= RULE_SQUARE_RIGHT_BRACKET otherlv_11= 'the' otherlv_12= 'payment' otherlv_13= 'network' this_SQUARE_LEFT_BRACKET_14= RULE_SQUARE_LEFT_BRACKET ( (lv_paymentnetwork_15_0= rulePaymentNetwork ) ) (this_COMMA_16= RULE_COMMA ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) ) )* this_SQUARE_RIGHT_BRACKET_18= RULE_SQUARE_RIGHT_BRACKET )
            {
            // InternalPaymentPlatform.g:78:2: ( () otherlv_1= 'In' ( (lv_name_2_0= ruleEString ) ) this_COLON_3= RULE_COLON otherlv_4= 'the' otherlv_5= 'adapter' this_SQUARE_LEFT_BRACKET_6= RULE_SQUARE_LEFT_BRACKET ( (lv_adapter_7_0= ruleAdapter ) ) (this_COMMA_8= RULE_COMMA ( (lv_adapter_9_0= ruleAdapter ) ) )* this_SQUARE_RIGHT_BRACKET_10= RULE_SQUARE_RIGHT_BRACKET otherlv_11= 'the' otherlv_12= 'payment' otherlv_13= 'network' this_SQUARE_LEFT_BRACKET_14= RULE_SQUARE_LEFT_BRACKET ( (lv_paymentnetwork_15_0= rulePaymentNetwork ) ) (this_COMMA_16= RULE_COMMA ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) ) )* this_SQUARE_RIGHT_BRACKET_18= RULE_SQUARE_RIGHT_BRACKET )
            // InternalPaymentPlatform.g:79:3: () otherlv_1= 'In' ( (lv_name_2_0= ruleEString ) ) this_COLON_3= RULE_COLON otherlv_4= 'the' otherlv_5= 'adapter' this_SQUARE_LEFT_BRACKET_6= RULE_SQUARE_LEFT_BRACKET ( (lv_adapter_7_0= ruleAdapter ) ) (this_COMMA_8= RULE_COMMA ( (lv_adapter_9_0= ruleAdapter ) ) )* this_SQUARE_RIGHT_BRACKET_10= RULE_SQUARE_RIGHT_BRACKET otherlv_11= 'the' otherlv_12= 'payment' otherlv_13= 'network' this_SQUARE_LEFT_BRACKET_14= RULE_SQUARE_LEFT_BRACKET ( (lv_paymentnetwork_15_0= rulePaymentNetwork ) ) (this_COMMA_16= RULE_COMMA ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) ) )* this_SQUARE_RIGHT_BRACKET_18= RULE_SQUARE_RIGHT_BRACKET
            {
            // InternalPaymentPlatform.g:79:3: ()
            // InternalPaymentPlatform.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPaymentPlaformAccess().getPaymentPlaformAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,23,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPaymentPlaformAccess().getInKeyword_1());
            		
            // InternalPaymentPlatform.g:90:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPaymentPlatform.g:91:4: (lv_name_2_0= ruleEString )
            {
            // InternalPaymentPlatform.g:91:4: (lv_name_2_0= ruleEString )
            // InternalPaymentPlatform.g:92:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPaymentPlaformAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPaymentPlaformRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_COLON_3=(Token)match(input,RULE_COLON,FOLLOW_5); 

            			newLeafNode(this_COLON_3, grammarAccess.getPaymentPlaformAccess().getCOLONTerminalRuleCall_3());
            		
            otherlv_4=(Token)match(input,24,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getPaymentPlaformAccess().getTheKeyword_4());
            		
            otherlv_5=(Token)match(input,25,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getPaymentPlaformAccess().getAdapterKeyword_5());
            		
            this_SQUARE_LEFT_BRACKET_6=(Token)match(input,RULE_SQUARE_LEFT_BRACKET,FOLLOW_3); 

            			newLeafNode(this_SQUARE_LEFT_BRACKET_6, grammarAccess.getPaymentPlaformAccess().getSQUARE_LEFT_BRACKETTerminalRuleCall_6());
            		
            // InternalPaymentPlatform.g:125:3: ( (lv_adapter_7_0= ruleAdapter ) )
            // InternalPaymentPlatform.g:126:4: (lv_adapter_7_0= ruleAdapter )
            {
            // InternalPaymentPlatform.g:126:4: (lv_adapter_7_0= ruleAdapter )
            // InternalPaymentPlatform.g:127:5: lv_adapter_7_0= ruleAdapter
            {

            					newCompositeNode(grammarAccess.getPaymentPlaformAccess().getAdapterAdapterParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_8);
            lv_adapter_7_0=ruleAdapter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPaymentPlaformRule());
            					}
            					add(
            						current,
            						"adapter",
            						lv_adapter_7_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.Adapter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPaymentPlatform.g:144:3: (this_COMMA_8= RULE_COMMA ( (lv_adapter_9_0= ruleAdapter ) ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_COMMA) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalPaymentPlatform.g:145:4: this_COMMA_8= RULE_COMMA ( (lv_adapter_9_0= ruleAdapter ) )
            	    {
            	    this_COMMA_8=(Token)match(input,RULE_COMMA,FOLLOW_3); 

            	    				newLeafNode(this_COMMA_8, grammarAccess.getPaymentPlaformAccess().getCOMMATerminalRuleCall_8_0());
            	    			
            	    // InternalPaymentPlatform.g:149:4: ( (lv_adapter_9_0= ruleAdapter ) )
            	    // InternalPaymentPlatform.g:150:5: (lv_adapter_9_0= ruleAdapter )
            	    {
            	    // InternalPaymentPlatform.g:150:5: (lv_adapter_9_0= ruleAdapter )
            	    // InternalPaymentPlatform.g:151:6: lv_adapter_9_0= ruleAdapter
            	    {

            	    						newCompositeNode(grammarAccess.getPaymentPlaformAccess().getAdapterAdapterParserRuleCall_8_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_adapter_9_0=ruleAdapter();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPaymentPlaformRule());
            	    						}
            	    						add(
            	    							current,
            	    							"adapter",
            	    							lv_adapter_9_0,
            	    							"com.payulatam.paymentplatform.xtext.PaymentPlatform.Adapter");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            this_SQUARE_RIGHT_BRACKET_10=(Token)match(input,RULE_SQUARE_RIGHT_BRACKET,FOLLOW_5); 

            			newLeafNode(this_SQUARE_RIGHT_BRACKET_10, grammarAccess.getPaymentPlaformAccess().getSQUARE_RIGHT_BRACKETTerminalRuleCall_9());
            		
            otherlv_11=(Token)match(input,24,FOLLOW_9); 

            			newLeafNode(otherlv_11, grammarAccess.getPaymentPlaformAccess().getTheKeyword_10());
            		
            otherlv_12=(Token)match(input,26,FOLLOW_10); 

            			newLeafNode(otherlv_12, grammarAccess.getPaymentPlaformAccess().getPaymentKeyword_11());
            		
            otherlv_13=(Token)match(input,27,FOLLOW_7); 

            			newLeafNode(otherlv_13, grammarAccess.getPaymentPlaformAccess().getNetworkKeyword_12());
            		
            this_SQUARE_LEFT_BRACKET_14=(Token)match(input,RULE_SQUARE_LEFT_BRACKET,FOLLOW_11); 

            			newLeafNode(this_SQUARE_LEFT_BRACKET_14, grammarAccess.getPaymentPlaformAccess().getSQUARE_LEFT_BRACKETTerminalRuleCall_13());
            		
            // InternalPaymentPlatform.g:189:3: ( (lv_paymentnetwork_15_0= rulePaymentNetwork ) )
            // InternalPaymentPlatform.g:190:4: (lv_paymentnetwork_15_0= rulePaymentNetwork )
            {
            // InternalPaymentPlatform.g:190:4: (lv_paymentnetwork_15_0= rulePaymentNetwork )
            // InternalPaymentPlatform.g:191:5: lv_paymentnetwork_15_0= rulePaymentNetwork
            {

            					newCompositeNode(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkPaymentNetworkParserRuleCall_14_0());
            				
            pushFollow(FOLLOW_8);
            lv_paymentnetwork_15_0=rulePaymentNetwork();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPaymentPlaformRule());
            					}
            					add(
            						current,
            						"paymentnetwork",
            						lv_paymentnetwork_15_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.PaymentNetwork");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPaymentPlatform.g:208:3: (this_COMMA_16= RULE_COMMA ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_COMMA) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalPaymentPlatform.g:209:4: this_COMMA_16= RULE_COMMA ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) )
            	    {
            	    this_COMMA_16=(Token)match(input,RULE_COMMA,FOLLOW_11); 

            	    				newLeafNode(this_COMMA_16, grammarAccess.getPaymentPlaformAccess().getCOMMATerminalRuleCall_15_0());
            	    			
            	    // InternalPaymentPlatform.g:213:4: ( (lv_paymentnetwork_17_0= rulePaymentNetwork ) )
            	    // InternalPaymentPlatform.g:214:5: (lv_paymentnetwork_17_0= rulePaymentNetwork )
            	    {
            	    // InternalPaymentPlatform.g:214:5: (lv_paymentnetwork_17_0= rulePaymentNetwork )
            	    // InternalPaymentPlatform.g:215:6: lv_paymentnetwork_17_0= rulePaymentNetwork
            	    {

            	    						newCompositeNode(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkPaymentNetworkParserRuleCall_15_1_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_paymentnetwork_17_0=rulePaymentNetwork();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPaymentPlaformRule());
            	    						}
            	    						add(
            	    							current,
            	    							"paymentnetwork",
            	    							lv_paymentnetwork_17_0,
            	    							"com.payulatam.paymentplatform.xtext.PaymentPlatform.PaymentNetwork");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            this_SQUARE_RIGHT_BRACKET_18=(Token)match(input,RULE_SQUARE_RIGHT_BRACKET,FOLLOW_2); 

            			newLeafNode(this_SQUARE_RIGHT_BRACKET_18, grammarAccess.getPaymentPlaformAccess().getSQUARE_RIGHT_BRACKETTerminalRuleCall_16());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePaymentPlaform"


    // $ANTLR start "entryRulePaymentNetwork"
    // InternalPaymentPlatform.g:241:1: entryRulePaymentNetwork returns [EObject current=null] : iv_rulePaymentNetwork= rulePaymentNetwork EOF ;
    public final EObject entryRulePaymentNetwork() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePaymentNetwork = null;


        try {
            // InternalPaymentPlatform.g:241:55: (iv_rulePaymentNetwork= rulePaymentNetwork EOF )
            // InternalPaymentPlatform.g:242:2: iv_rulePaymentNetwork= rulePaymentNetwork EOF
            {
             newCompositeNode(grammarAccess.getPaymentNetworkRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePaymentNetwork=rulePaymentNetwork();

            state._fsp--;

             current =iv_rulePaymentNetwork; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePaymentNetwork"


    // $ANTLR start "rulePaymentNetwork"
    // InternalPaymentPlatform.g:248:1: rulePaymentNetwork returns [EObject current=null] : (this_RestPaymentNetwork_0= ruleRestPaymentNetwork | this_SOAPPaymentNetwork_1= ruleSOAPPaymentNetwork ) ;
    public final EObject rulePaymentNetwork() throws RecognitionException {
        EObject current = null;

        EObject this_RestPaymentNetwork_0 = null;

        EObject this_SOAPPaymentNetwork_1 = null;



        	enterRule();

        try {
            // InternalPaymentPlatform.g:254:2: ( (this_RestPaymentNetwork_0= ruleRestPaymentNetwork | this_SOAPPaymentNetwork_1= ruleSOAPPaymentNetwork ) )
            // InternalPaymentPlatform.g:255:2: (this_RestPaymentNetwork_0= ruleRestPaymentNetwork | this_SOAPPaymentNetwork_1= ruleSOAPPaymentNetwork )
            {
            // InternalPaymentPlatform.g:255:2: (this_RestPaymentNetwork_0= ruleRestPaymentNetwork | this_SOAPPaymentNetwork_1= ruleSOAPPaymentNetwork )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==33) ) {
                alt3=1;
            }
            else if ( (LA3_0==34) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalPaymentPlatform.g:256:3: this_RestPaymentNetwork_0= ruleRestPaymentNetwork
                    {

                    			newCompositeNode(grammarAccess.getPaymentNetworkAccess().getRestPaymentNetworkParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_RestPaymentNetwork_0=ruleRestPaymentNetwork();

                    state._fsp--;


                    			current = this_RestPaymentNetwork_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPaymentPlatform.g:265:3: this_SOAPPaymentNetwork_1= ruleSOAPPaymentNetwork
                    {

                    			newCompositeNode(grammarAccess.getPaymentNetworkAccess().getSOAPPaymentNetworkParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SOAPPaymentNetwork_1=ruleSOAPPaymentNetwork();

                    state._fsp--;


                    			current = this_SOAPPaymentNetwork_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePaymentNetwork"


    // $ANTLR start "entryRuleEString"
    // InternalPaymentPlatform.g:277:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalPaymentPlatform.g:277:47: (iv_ruleEString= ruleEString EOF )
            // InternalPaymentPlatform.g:278:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalPaymentPlatform.g:284:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalPaymentPlatform.g:290:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalPaymentPlatform.g:291:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalPaymentPlatform.g:291:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_STRING) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalPaymentPlatform.g:292:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPaymentPlatform.g:300:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleAdapter"
    // InternalPaymentPlatform.g:311:1: entryRuleAdapter returns [EObject current=null] : iv_ruleAdapter= ruleAdapter EOF ;
    public final EObject entryRuleAdapter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdapter = null;


        try {
            // InternalPaymentPlatform.g:311:48: (iv_ruleAdapter= ruleAdapter EOF )
            // InternalPaymentPlatform.g:312:2: iv_ruleAdapter= ruleAdapter EOF
            {
             newCompositeNode(grammarAccess.getAdapterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAdapter=ruleAdapter();

            state._fsp--;

             current =iv_ruleAdapter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdapter"


    // $ANTLR start "ruleAdapter"
    // InternalPaymentPlatform.g:318:1: ruleAdapter returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) this_LEFT_BRACKET_1= RULE_LEFT_BRACKET otherlv_2= 'uses' ( ( ruleEString ) ) otherlv_4= 'check' otherlv_5= 'in' ( (lv_repository_6_0= ruleRepository ) ) this_RIGHT_BRACKET_7= RULE_RIGHT_BRACKET ) ;
    public final EObject ruleAdapter() throws RecognitionException {
        EObject current = null;

        Token this_LEFT_BRACKET_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token this_RIGHT_BRACKET_7=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_repository_6_0 = null;



        	enterRule();

        try {
            // InternalPaymentPlatform.g:324:2: ( ( ( (lv_name_0_0= ruleEString ) ) this_LEFT_BRACKET_1= RULE_LEFT_BRACKET otherlv_2= 'uses' ( ( ruleEString ) ) otherlv_4= 'check' otherlv_5= 'in' ( (lv_repository_6_0= ruleRepository ) ) this_RIGHT_BRACKET_7= RULE_RIGHT_BRACKET ) )
            // InternalPaymentPlatform.g:325:2: ( ( (lv_name_0_0= ruleEString ) ) this_LEFT_BRACKET_1= RULE_LEFT_BRACKET otherlv_2= 'uses' ( ( ruleEString ) ) otherlv_4= 'check' otherlv_5= 'in' ( (lv_repository_6_0= ruleRepository ) ) this_RIGHT_BRACKET_7= RULE_RIGHT_BRACKET )
            {
            // InternalPaymentPlatform.g:325:2: ( ( (lv_name_0_0= ruleEString ) ) this_LEFT_BRACKET_1= RULE_LEFT_BRACKET otherlv_2= 'uses' ( ( ruleEString ) ) otherlv_4= 'check' otherlv_5= 'in' ( (lv_repository_6_0= ruleRepository ) ) this_RIGHT_BRACKET_7= RULE_RIGHT_BRACKET )
            // InternalPaymentPlatform.g:326:3: ( (lv_name_0_0= ruleEString ) ) this_LEFT_BRACKET_1= RULE_LEFT_BRACKET otherlv_2= 'uses' ( ( ruleEString ) ) otherlv_4= 'check' otherlv_5= 'in' ( (lv_repository_6_0= ruleRepository ) ) this_RIGHT_BRACKET_7= RULE_RIGHT_BRACKET
            {
            // InternalPaymentPlatform.g:326:3: ( (lv_name_0_0= ruleEString ) )
            // InternalPaymentPlatform.g:327:4: (lv_name_0_0= ruleEString )
            {
            // InternalPaymentPlatform.g:327:4: (lv_name_0_0= ruleEString )
            // InternalPaymentPlatform.g:328:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAdapterAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_12);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAdapterRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_LEFT_BRACKET_1=(Token)match(input,RULE_LEFT_BRACKET,FOLLOW_13); 

            			newLeafNode(this_LEFT_BRACKET_1, grammarAccess.getAdapterAccess().getLEFT_BRACKETTerminalRuleCall_1());
            		
            otherlv_2=(Token)match(input,28,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getAdapterAccess().getUsesKeyword_2());
            		
            // InternalPaymentPlatform.g:353:3: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:354:4: ( ruleEString )
            {
            // InternalPaymentPlatform.g:354:4: ( ruleEString )
            // InternalPaymentPlatform.g:355:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAdapterRule());
            					}
            				

            					newCompositeNode(grammarAccess.getAdapterAccess().getPaymentnetworkPaymentNetworkCrossReference_3_0());
            				
            pushFollow(FOLLOW_14);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,29,FOLLOW_15); 

            			newLeafNode(otherlv_4, grammarAccess.getAdapterAccess().getCheckKeyword_4());
            		
            otherlv_5=(Token)match(input,30,FOLLOW_12); 

            			newLeafNode(otherlv_5, grammarAccess.getAdapterAccess().getInKeyword_5());
            		
            // InternalPaymentPlatform.g:377:3: ( (lv_repository_6_0= ruleRepository ) )
            // InternalPaymentPlatform.g:378:4: (lv_repository_6_0= ruleRepository )
            {
            // InternalPaymentPlatform.g:378:4: (lv_repository_6_0= ruleRepository )
            // InternalPaymentPlatform.g:379:5: lv_repository_6_0= ruleRepository
            {

            					newCompositeNode(grammarAccess.getAdapterAccess().getRepositoryRepositoryParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_16);
            lv_repository_6_0=ruleRepository();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAdapterRule());
            					}
            					set(
            						current,
            						"repository",
            						lv_repository_6_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.Repository");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            this_RIGHT_BRACKET_7=(Token)match(input,RULE_RIGHT_BRACKET,FOLLOW_2); 

            			newLeafNode(this_RIGHT_BRACKET_7, grammarAccess.getAdapterAccess().getRIGHT_BRACKETTerminalRuleCall_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdapter"


    // $ANTLR start "entryRuleRepository"
    // InternalPaymentPlatform.g:404:1: entryRuleRepository returns [EObject current=null] : iv_ruleRepository= ruleRepository EOF ;
    public final EObject entryRuleRepository() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepository = null;


        try {
            // InternalPaymentPlatform.g:404:51: (iv_ruleRepository= ruleRepository EOF )
            // InternalPaymentPlatform.g:405:2: iv_ruleRepository= ruleRepository EOF
            {
             newCompositeNode(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRepository=ruleRepository();

            state._fsp--;

             current =iv_ruleRepository; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalPaymentPlatform.g:411:1: ruleRepository returns [EObject current=null] : (this_LEFT_BRACKET_0= RULE_LEFT_BRACKET otherlv_1= 'url' ( (lv_url_2_0= ruleEString ) ) (otherlv_3= 'branch' ( (lv_branch_4_0= ruleEString ) ) )? this_RIGHT_BRACKET_5= RULE_RIGHT_BRACKET ) ;
    public final EObject ruleRepository() throws RecognitionException {
        EObject current = null;

        Token this_LEFT_BRACKET_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token this_RIGHT_BRACKET_5=null;
        AntlrDatatypeRuleToken lv_url_2_0 = null;

        AntlrDatatypeRuleToken lv_branch_4_0 = null;



        	enterRule();

        try {
            // InternalPaymentPlatform.g:417:2: ( (this_LEFT_BRACKET_0= RULE_LEFT_BRACKET otherlv_1= 'url' ( (lv_url_2_0= ruleEString ) ) (otherlv_3= 'branch' ( (lv_branch_4_0= ruleEString ) ) )? this_RIGHT_BRACKET_5= RULE_RIGHT_BRACKET ) )
            // InternalPaymentPlatform.g:418:2: (this_LEFT_BRACKET_0= RULE_LEFT_BRACKET otherlv_1= 'url' ( (lv_url_2_0= ruleEString ) ) (otherlv_3= 'branch' ( (lv_branch_4_0= ruleEString ) ) )? this_RIGHT_BRACKET_5= RULE_RIGHT_BRACKET )
            {
            // InternalPaymentPlatform.g:418:2: (this_LEFT_BRACKET_0= RULE_LEFT_BRACKET otherlv_1= 'url' ( (lv_url_2_0= ruleEString ) ) (otherlv_3= 'branch' ( (lv_branch_4_0= ruleEString ) ) )? this_RIGHT_BRACKET_5= RULE_RIGHT_BRACKET )
            // InternalPaymentPlatform.g:419:3: this_LEFT_BRACKET_0= RULE_LEFT_BRACKET otherlv_1= 'url' ( (lv_url_2_0= ruleEString ) ) (otherlv_3= 'branch' ( (lv_branch_4_0= ruleEString ) ) )? this_RIGHT_BRACKET_5= RULE_RIGHT_BRACKET
            {
            this_LEFT_BRACKET_0=(Token)match(input,RULE_LEFT_BRACKET,FOLLOW_17); 

            			newLeafNode(this_LEFT_BRACKET_0, grammarAccess.getRepositoryAccess().getLEFT_BRACKETTerminalRuleCall_0());
            		
            otherlv_1=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getRepositoryAccess().getUrlKeyword_1());
            		
            // InternalPaymentPlatform.g:427:3: ( (lv_url_2_0= ruleEString ) )
            // InternalPaymentPlatform.g:428:4: (lv_url_2_0= ruleEString )
            {
            // InternalPaymentPlatform.g:428:4: (lv_url_2_0= ruleEString )
            // InternalPaymentPlatform.g:429:5: lv_url_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRepositoryAccess().getUrlEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_18);
            lv_url_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRepositoryRule());
            					}
            					set(
            						current,
            						"url",
            						lv_url_2_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalPaymentPlatform.g:446:3: (otherlv_3= 'branch' ( (lv_branch_4_0= ruleEString ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==32) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalPaymentPlatform.g:447:4: otherlv_3= 'branch' ( (lv_branch_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,32,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getRepositoryAccess().getBranchKeyword_3_0());
                    			
                    // InternalPaymentPlatform.g:451:4: ( (lv_branch_4_0= ruleEString ) )
                    // InternalPaymentPlatform.g:452:5: (lv_branch_4_0= ruleEString )
                    {
                    // InternalPaymentPlatform.g:452:5: (lv_branch_4_0= ruleEString )
                    // InternalPaymentPlatform.g:453:6: lv_branch_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getRepositoryAccess().getBranchEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_16);
                    lv_branch_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRepositoryRule());
                    						}
                    						set(
                    							current,
                    							"branch",
                    							lv_branch_4_0,
                    							"com.payulatam.paymentplatform.xtext.PaymentPlatform.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            this_RIGHT_BRACKET_5=(Token)match(input,RULE_RIGHT_BRACKET,FOLLOW_2); 

            			newLeafNode(this_RIGHT_BRACKET_5, grammarAccess.getRepositoryAccess().getRIGHT_BRACKETTerminalRuleCall_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleRestPaymentNetwork"
    // InternalPaymentPlatform.g:479:1: entryRuleRestPaymentNetwork returns [EObject current=null] : iv_ruleRestPaymentNetwork= ruleRestPaymentNetwork EOF ;
    public final EObject entryRuleRestPaymentNetwork() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRestPaymentNetwork = null;


        try {
            // InternalPaymentPlatform.g:479:59: (iv_ruleRestPaymentNetwork= ruleRestPaymentNetwork EOF )
            // InternalPaymentPlatform.g:480:2: iv_ruleRestPaymentNetwork= ruleRestPaymentNetwork EOF
            {
             newCompositeNode(grammarAccess.getRestPaymentNetworkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRestPaymentNetwork=ruleRestPaymentNetwork();

            state._fsp--;

             current =iv_ruleRestPaymentNetwork; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRestPaymentNetwork"


    // $ANTLR start "ruleRestPaymentNetwork"
    // InternalPaymentPlatform.g:486:1: ruleRestPaymentNetwork returns [EObject current=null] : ( () otherlv_1= 'RestPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleRestPaymentNetwork() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalPaymentPlatform.g:492:2: ( ( () otherlv_1= 'RestPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalPaymentPlatform.g:493:2: ( () otherlv_1= 'RestPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalPaymentPlatform.g:493:2: ( () otherlv_1= 'RestPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) )
            // InternalPaymentPlatform.g:494:3: () otherlv_1= 'RestPaymentNetwork' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalPaymentPlatform.g:494:3: ()
            // InternalPaymentPlatform.g:495:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRestPaymentNetworkAccess().getRestPaymentNetworkAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,33,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getRestPaymentNetworkAccess().getRestPaymentNetworkKeyword_1());
            		
            // InternalPaymentPlatform.g:505:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPaymentPlatform.g:506:4: (lv_name_2_0= ruleEString )
            {
            // InternalPaymentPlatform.g:506:4: (lv_name_2_0= ruleEString )
            // InternalPaymentPlatform.g:507:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getRestPaymentNetworkAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRestPaymentNetworkRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRestPaymentNetwork"


    // $ANTLR start "entryRuleSOAPPaymentNetwork"
    // InternalPaymentPlatform.g:528:1: entryRuleSOAPPaymentNetwork returns [EObject current=null] : iv_ruleSOAPPaymentNetwork= ruleSOAPPaymentNetwork EOF ;
    public final EObject entryRuleSOAPPaymentNetwork() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSOAPPaymentNetwork = null;


        try {
            // InternalPaymentPlatform.g:528:59: (iv_ruleSOAPPaymentNetwork= ruleSOAPPaymentNetwork EOF )
            // InternalPaymentPlatform.g:529:2: iv_ruleSOAPPaymentNetwork= ruleSOAPPaymentNetwork EOF
            {
             newCompositeNode(grammarAccess.getSOAPPaymentNetworkRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSOAPPaymentNetwork=ruleSOAPPaymentNetwork();

            state._fsp--;

             current =iv_ruleSOAPPaymentNetwork; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSOAPPaymentNetwork"


    // $ANTLR start "ruleSOAPPaymentNetwork"
    // InternalPaymentPlatform.g:535:1: ruleSOAPPaymentNetwork returns [EObject current=null] : ( () otherlv_1= 'SOAPPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleSOAPPaymentNetwork() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalPaymentPlatform.g:541:2: ( ( () otherlv_1= 'SOAPPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalPaymentPlatform.g:542:2: ( () otherlv_1= 'SOAPPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalPaymentPlatform.g:542:2: ( () otherlv_1= 'SOAPPaymentNetwork' ( (lv_name_2_0= ruleEString ) ) )
            // InternalPaymentPlatform.g:543:3: () otherlv_1= 'SOAPPaymentNetwork' ( (lv_name_2_0= ruleEString ) )
            {
            // InternalPaymentPlatform.g:543:3: ()
            // InternalPaymentPlatform.g:544:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSOAPPaymentNetworkAccess().getSOAPPaymentNetworkAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getSOAPPaymentNetworkAccess().getSOAPPaymentNetworkKeyword_1());
            		
            // InternalPaymentPlatform.g:554:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPaymentPlatform.g:555:4: (lv_name_2_0= ruleEString )
            {
            // InternalPaymentPlatform.g:555:4: (lv_name_2_0= ruleEString )
            // InternalPaymentPlatform.g:556:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSOAPPaymentNetworkAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSOAPPaymentNetworkRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.payulatam.paymentplatform.xtext.PaymentPlatform.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSOAPPaymentNetwork"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000300L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000000000000C0L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000100000800L});

}