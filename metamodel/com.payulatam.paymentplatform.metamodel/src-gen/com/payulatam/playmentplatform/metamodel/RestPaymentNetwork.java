/**
 */
package com.payulatam.playmentplatform.metamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rest Payment Network</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getRestPaymentNetwork()
 * @model
 * @generated
 */
public interface RestPaymentNetwork extends PaymentNetwork {
} // RestPaymentNetwork
