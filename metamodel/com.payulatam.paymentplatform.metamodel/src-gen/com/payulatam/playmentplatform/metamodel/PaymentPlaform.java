/**
 */
package com.payulatam.playmentplatform.metamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Payment Plaform</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform#getName <em>Name</em>}</li>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform#getAdapter <em>Adapter</em>}</li>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform#getPaymentnetwork <em>Paymentnetwork</em>}</li>
 * </ul>
 *
 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getPaymentPlaform()
 * @model
 * @generated
 */
public interface PaymentPlaform extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getPaymentPlaform_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Adapter</b></em>' containment reference list.
	 * The list contents are of type {@link com.payulatam.playmentplatform.metamodel.Adapter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adapter</em>' containment reference list.
	 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getPaymentPlaform_Adapter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Adapter> getAdapter();

	/**
	 * Returns the value of the '<em><b>Paymentnetwork</b></em>' containment reference list.
	 * The list contents are of type {@link com.payulatam.playmentplatform.metamodel.PaymentNetwork}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paymentnetwork</em>' containment reference list.
	 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getPaymentPlaform_Paymentnetwork()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<PaymentNetwork> getPaymentnetwork();

} // PaymentPlaform
