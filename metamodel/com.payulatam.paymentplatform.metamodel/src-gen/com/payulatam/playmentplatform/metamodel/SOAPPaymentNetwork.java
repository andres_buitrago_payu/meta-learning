/**
 */
package com.payulatam.playmentplatform.metamodel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SOAP Payment Network</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getSOAPPaymentNetwork()
 * @model
 * @generated
 */
public interface SOAPPaymentNetwork extends PaymentNetwork {
} // SOAPPaymentNetwork
