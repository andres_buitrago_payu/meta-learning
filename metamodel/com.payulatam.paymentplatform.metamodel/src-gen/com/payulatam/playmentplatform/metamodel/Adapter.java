/**
 */
package com.payulatam.playmentplatform.metamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.Adapter#getName <em>Name</em>}</li>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.Adapter#getPaymentnetwork <em>Paymentnetwork</em>}</li>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.Adapter#getRepository <em>Repository</em>}</li>
 * </ul>
 *
 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getAdapter()
 * @model
 * @generated
 */
public interface Adapter extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getAdapter_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link com.payulatam.playmentplatform.metamodel.Adapter#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Paymentnetwork</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paymentnetwork</em>' reference.
	 * @see #setPaymentnetwork(PaymentNetwork)
	 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getAdapter_Paymentnetwork()
	 * @model required="true"
	 * @generated
	 */
	PaymentNetwork getPaymentnetwork();

	/**
	 * Sets the value of the '{@link com.payulatam.playmentplatform.metamodel.Adapter#getPaymentnetwork <em>Paymentnetwork</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Paymentnetwork</em>' reference.
	 * @see #getPaymentnetwork()
	 * @generated
	 */
	void setPaymentnetwork(PaymentNetwork value);

	/**
	 * Returns the value of the '<em><b>Repository</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repository</em>' containment reference.
	 * @see #setRepository(Repository)
	 * @see com.payulatam.playmentplatform.metamodel.MetamodelPackage#getAdapter_Repository()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Repository getRepository();

	/**
	 * Sets the value of the '{@link com.payulatam.playmentplatform.metamodel.Adapter#getRepository <em>Repository</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repository</em>' containment reference.
	 * @see #getRepository()
	 * @generated
	 */
	void setRepository(Repository value);

} // Adapter
