/**
 */
package com.payulatam.playmentplatform.metamodel.impl;

import com.payulatam.playmentplatform.metamodel.Adapter;
import com.payulatam.playmentplatform.metamodel.MetamodelPackage;
import com.payulatam.playmentplatform.metamodel.PaymentNetwork;
import com.payulatam.playmentplatform.metamodel.PaymentPlaform;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Payment Plaform</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.impl.PaymentPlaformImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.impl.PaymentPlaformImpl#getAdapter <em>Adapter</em>}</li>
 *   <li>{@link com.payulatam.playmentplatform.metamodel.impl.PaymentPlaformImpl#getPaymentnetwork <em>Paymentnetwork</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PaymentPlaformImpl extends MinimalEObjectImpl.Container implements PaymentPlaform {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAdapter() <em>Adapter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdapter()
	 * @generated
	 * @ordered
	 */
	protected EList<Adapter> adapter;

	/**
	 * The cached value of the '{@link #getPaymentnetwork() <em>Paymentnetwork</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPaymentnetwork()
	 * @generated
	 * @ordered
	 */
	protected EList<PaymentNetwork> paymentnetwork;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PaymentPlaformImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.PAYMENT_PLAFORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.PAYMENT_PLAFORM__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Adapter> getAdapter() {
		if (adapter == null) {
			adapter = new EObjectContainmentEList<Adapter>(Adapter.class, this,
					MetamodelPackage.PAYMENT_PLAFORM__ADAPTER);
		}
		return adapter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PaymentNetwork> getPaymentnetwork() {
		if (paymentnetwork == null) {
			paymentnetwork = new EObjectContainmentEList<PaymentNetwork>(PaymentNetwork.class, this,
					MetamodelPackage.PAYMENT_PLAFORM__PAYMENTNETWORK);
		}
		return paymentnetwork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MetamodelPackage.PAYMENT_PLAFORM__ADAPTER:
			return ((InternalEList<?>) getAdapter()).basicRemove(otherEnd, msgs);
		case MetamodelPackage.PAYMENT_PLAFORM__PAYMENTNETWORK:
			return ((InternalEList<?>) getPaymentnetwork()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MetamodelPackage.PAYMENT_PLAFORM__NAME:
			return getName();
		case MetamodelPackage.PAYMENT_PLAFORM__ADAPTER:
			return getAdapter();
		case MetamodelPackage.PAYMENT_PLAFORM__PAYMENTNETWORK:
			return getPaymentnetwork();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MetamodelPackage.PAYMENT_PLAFORM__NAME:
			setName((String) newValue);
			return;
		case MetamodelPackage.PAYMENT_PLAFORM__ADAPTER:
			getAdapter().clear();
			getAdapter().addAll((Collection<? extends Adapter>) newValue);
			return;
		case MetamodelPackage.PAYMENT_PLAFORM__PAYMENTNETWORK:
			getPaymentnetwork().clear();
			getPaymentnetwork().addAll((Collection<? extends PaymentNetwork>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MetamodelPackage.PAYMENT_PLAFORM__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MetamodelPackage.PAYMENT_PLAFORM__ADAPTER:
			getAdapter().clear();
			return;
		case MetamodelPackage.PAYMENT_PLAFORM__PAYMENTNETWORK:
			getPaymentnetwork().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MetamodelPackage.PAYMENT_PLAFORM__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MetamodelPackage.PAYMENT_PLAFORM__ADAPTER:
			return adapter != null && !adapter.isEmpty();
		case MetamodelPackage.PAYMENT_PLAFORM__PAYMENTNETWORK:
			return paymentnetwork != null && !paymentnetwork.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PaymentPlaformImpl
