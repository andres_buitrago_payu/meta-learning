/**
 */
package com.payulatam.playmentplatform.metamodel.impl;

import com.payulatam.playmentplatform.metamodel.MetamodelPackage;
import com.payulatam.playmentplatform.metamodel.RestPaymentNetwork;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rest Payment Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RestPaymentNetworkImpl extends PaymentNetworkImpl implements RestPaymentNetwork {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestPaymentNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.REST_PAYMENT_NETWORK;
	}

} //RestPaymentNetworkImpl
