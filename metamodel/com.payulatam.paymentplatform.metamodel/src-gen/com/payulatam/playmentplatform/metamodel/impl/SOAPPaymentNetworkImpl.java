/**
 */
package com.payulatam.playmentplatform.metamodel.impl;

import com.payulatam.playmentplatform.metamodel.MetamodelPackage;
import com.payulatam.playmentplatform.metamodel.SOAPPaymentNetwork;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SOAP Payment Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SOAPPaymentNetworkImpl extends PaymentNetworkImpl implements SOAPPaymentNetwork {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SOAPPaymentNetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.SOAP_PAYMENT_NETWORK;
	}

} //SOAPPaymentNetworkImpl
