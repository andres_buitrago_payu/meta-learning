/**
 */
package com.payulatam.playmentplatform.metamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.payulatam.playmentplatform.metamodel.MetamodelFactory
 * @model kind="package"
 * @generated
 */
public interface MetamodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "metamodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://com.payulatam.playmentplatform/metamodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metamodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MetamodelPackage eINSTANCE = com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.payulatam.playmentplatform.metamodel.impl.PaymentPlaformImpl <em>Payment Plaform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.payulatam.playmentplatform.metamodel.impl.PaymentPlaformImpl
	 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getPaymentPlaform()
	 * @generated
	 */
	int PAYMENT_PLAFORM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_PLAFORM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Adapter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_PLAFORM__ADAPTER = 1;

	/**
	 * The feature id for the '<em><b>Paymentnetwork</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_PLAFORM__PAYMENTNETWORK = 2;

	/**
	 * The number of structural features of the '<em>Payment Plaform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_PLAFORM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Payment Plaform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_PLAFORM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.payulatam.playmentplatform.metamodel.impl.AdapterImpl <em>Adapter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.payulatam.playmentplatform.metamodel.impl.AdapterImpl
	 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getAdapter()
	 * @generated
	 */
	int ADAPTER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADAPTER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Paymentnetwork</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADAPTER__PAYMENTNETWORK = 1;

	/**
	 * The feature id for the '<em><b>Repository</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADAPTER__REPOSITORY = 2;

	/**
	 * The number of structural features of the '<em>Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADAPTER_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADAPTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.payulatam.playmentplatform.metamodel.impl.PaymentNetworkImpl <em>Payment Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.payulatam.playmentplatform.metamodel.impl.PaymentNetworkImpl
	 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getPaymentNetwork()
	 * @generated
	 */
	int PAYMENT_NETWORK = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_NETWORK__NAME = 0;

	/**
	 * The number of structural features of the '<em>Payment Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_NETWORK_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Payment Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_NETWORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.payulatam.playmentplatform.metamodel.impl.RestPaymentNetworkImpl <em>Rest Payment Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.payulatam.playmentplatform.metamodel.impl.RestPaymentNetworkImpl
	 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getRestPaymentNetwork()
	 * @generated
	 */
	int REST_PAYMENT_NETWORK = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_PAYMENT_NETWORK__NAME = PAYMENT_NETWORK__NAME;

	/**
	 * The number of structural features of the '<em>Rest Payment Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_PAYMENT_NETWORK_FEATURE_COUNT = PAYMENT_NETWORK_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Rest Payment Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REST_PAYMENT_NETWORK_OPERATION_COUNT = PAYMENT_NETWORK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.payulatam.playmentplatform.metamodel.impl.SOAPPaymentNetworkImpl <em>SOAP Payment Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.payulatam.playmentplatform.metamodel.impl.SOAPPaymentNetworkImpl
	 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getSOAPPaymentNetwork()
	 * @generated
	 */
	int SOAP_PAYMENT_NETWORK = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOAP_PAYMENT_NETWORK__NAME = PAYMENT_NETWORK__NAME;

	/**
	 * The number of structural features of the '<em>SOAP Payment Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOAP_PAYMENT_NETWORK_FEATURE_COUNT = PAYMENT_NETWORK_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>SOAP Payment Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOAP_PAYMENT_NETWORK_OPERATION_COUNT = PAYMENT_NETWORK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.payulatam.playmentplatform.metamodel.impl.RepositoryImpl <em>Repository</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.payulatam.playmentplatform.metamodel.impl.RepositoryImpl
	 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getRepository()
	 * @generated
	 */
	int REPOSITORY = 5;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__URL = 0;

	/**
	 * The feature id for the '<em><b>Branch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY__BRANCH = 1;

	/**
	 * The number of structural features of the '<em>Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Repository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPOSITORY_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform <em>Payment Plaform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Payment Plaform</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.PaymentPlaform
	 * @generated
	 */
	EClass getPaymentPlaform();

	/**
	 * Returns the meta object for the attribute '{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.PaymentPlaform#getName()
	 * @see #getPaymentPlaform()
	 * @generated
	 */
	EAttribute getPaymentPlaform_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform#getAdapter <em>Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Adapter</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.PaymentPlaform#getAdapter()
	 * @see #getPaymentPlaform()
	 * @generated
	 */
	EReference getPaymentPlaform_Adapter();

	/**
	 * Returns the meta object for the containment reference list '{@link com.payulatam.playmentplatform.metamodel.PaymentPlaform#getPaymentnetwork <em>Paymentnetwork</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Paymentnetwork</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.PaymentPlaform#getPaymentnetwork()
	 * @see #getPaymentPlaform()
	 * @generated
	 */
	EReference getPaymentPlaform_Paymentnetwork();

	/**
	 * Returns the meta object for class '{@link com.payulatam.playmentplatform.metamodel.Adapter <em>Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Adapter</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.Adapter
	 * @generated
	 */
	EClass getAdapter();

	/**
	 * Returns the meta object for the attribute '{@link com.payulatam.playmentplatform.metamodel.Adapter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.Adapter#getName()
	 * @see #getAdapter()
	 * @generated
	 */
	EAttribute getAdapter_Name();

	/**
	 * Returns the meta object for the reference '{@link com.payulatam.playmentplatform.metamodel.Adapter#getPaymentnetwork <em>Paymentnetwork</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Paymentnetwork</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.Adapter#getPaymentnetwork()
	 * @see #getAdapter()
	 * @generated
	 */
	EReference getAdapter_Paymentnetwork();

	/**
	 * Returns the meta object for the containment reference '{@link com.payulatam.playmentplatform.metamodel.Adapter#getRepository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Repository</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.Adapter#getRepository()
	 * @see #getAdapter()
	 * @generated
	 */
	EReference getAdapter_Repository();

	/**
	 * Returns the meta object for class '{@link com.payulatam.playmentplatform.metamodel.PaymentNetwork <em>Payment Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Payment Network</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.PaymentNetwork
	 * @generated
	 */
	EClass getPaymentNetwork();

	/**
	 * Returns the meta object for the attribute '{@link com.payulatam.playmentplatform.metamodel.PaymentNetwork#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.PaymentNetwork#getName()
	 * @see #getPaymentNetwork()
	 * @generated
	 */
	EAttribute getPaymentNetwork_Name();

	/**
	 * Returns the meta object for class '{@link com.payulatam.playmentplatform.metamodel.RestPaymentNetwork <em>Rest Payment Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rest Payment Network</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.RestPaymentNetwork
	 * @generated
	 */
	EClass getRestPaymentNetwork();

	/**
	 * Returns the meta object for class '{@link com.payulatam.playmentplatform.metamodel.SOAPPaymentNetwork <em>SOAP Payment Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SOAP Payment Network</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.SOAPPaymentNetwork
	 * @generated
	 */
	EClass getSOAPPaymentNetwork();

	/**
	 * Returns the meta object for class '{@link com.payulatam.playmentplatform.metamodel.Repository <em>Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repository</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.Repository
	 * @generated
	 */
	EClass getRepository();

	/**
	 * Returns the meta object for the attribute '{@link com.payulatam.playmentplatform.metamodel.Repository#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.Repository#getUrl()
	 * @see #getRepository()
	 * @generated
	 */
	EAttribute getRepository_Url();

	/**
	 * Returns the meta object for the attribute '{@link com.payulatam.playmentplatform.metamodel.Repository#getBranch <em>Branch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Branch</em>'.
	 * @see com.payulatam.playmentplatform.metamodel.Repository#getBranch()
	 * @see #getRepository()
	 * @generated
	 */
	EAttribute getRepository_Branch();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MetamodelFactory getMetamodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.payulatam.playmentplatform.metamodel.impl.PaymentPlaformImpl <em>Payment Plaform</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.payulatam.playmentplatform.metamodel.impl.PaymentPlaformImpl
		 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getPaymentPlaform()
		 * @generated
		 */
		EClass PAYMENT_PLAFORM = eINSTANCE.getPaymentPlaform();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAYMENT_PLAFORM__NAME = eINSTANCE.getPaymentPlaform_Name();

		/**
		 * The meta object literal for the '<em><b>Adapter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAYMENT_PLAFORM__ADAPTER = eINSTANCE.getPaymentPlaform_Adapter();

		/**
		 * The meta object literal for the '<em><b>Paymentnetwork</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAYMENT_PLAFORM__PAYMENTNETWORK = eINSTANCE.getPaymentPlaform_Paymentnetwork();

		/**
		 * The meta object literal for the '{@link com.payulatam.playmentplatform.metamodel.impl.AdapterImpl <em>Adapter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.payulatam.playmentplatform.metamodel.impl.AdapterImpl
		 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getAdapter()
		 * @generated
		 */
		EClass ADAPTER = eINSTANCE.getAdapter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADAPTER__NAME = eINSTANCE.getAdapter_Name();

		/**
		 * The meta object literal for the '<em><b>Paymentnetwork</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADAPTER__PAYMENTNETWORK = eINSTANCE.getAdapter_Paymentnetwork();

		/**
		 * The meta object literal for the '<em><b>Repository</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADAPTER__REPOSITORY = eINSTANCE.getAdapter_Repository();

		/**
		 * The meta object literal for the '{@link com.payulatam.playmentplatform.metamodel.impl.PaymentNetworkImpl <em>Payment Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.payulatam.playmentplatform.metamodel.impl.PaymentNetworkImpl
		 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getPaymentNetwork()
		 * @generated
		 */
		EClass PAYMENT_NETWORK = eINSTANCE.getPaymentNetwork();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAYMENT_NETWORK__NAME = eINSTANCE.getPaymentNetwork_Name();

		/**
		 * The meta object literal for the '{@link com.payulatam.playmentplatform.metamodel.impl.RestPaymentNetworkImpl <em>Rest Payment Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.payulatam.playmentplatform.metamodel.impl.RestPaymentNetworkImpl
		 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getRestPaymentNetwork()
		 * @generated
		 */
		EClass REST_PAYMENT_NETWORK = eINSTANCE.getRestPaymentNetwork();

		/**
		 * The meta object literal for the '{@link com.payulatam.playmentplatform.metamodel.impl.SOAPPaymentNetworkImpl <em>SOAP Payment Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.payulatam.playmentplatform.metamodel.impl.SOAPPaymentNetworkImpl
		 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getSOAPPaymentNetwork()
		 * @generated
		 */
		EClass SOAP_PAYMENT_NETWORK = eINSTANCE.getSOAPPaymentNetwork();

		/**
		 * The meta object literal for the '{@link com.payulatam.playmentplatform.metamodel.impl.RepositoryImpl <em>Repository</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.payulatam.playmentplatform.metamodel.impl.RepositoryImpl
		 * @see com.payulatam.playmentplatform.metamodel.impl.MetamodelPackageImpl#getRepository()
		 * @generated
		 */
		EClass REPOSITORY = eINSTANCE.getRepository();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY__URL = eINSTANCE.getRepository_Url();

		/**
		 * The meta object literal for the '<em><b>Branch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPOSITORY__BRANCH = eINSTANCE.getRepository_Branch();

	}

} //MetamodelPackage
