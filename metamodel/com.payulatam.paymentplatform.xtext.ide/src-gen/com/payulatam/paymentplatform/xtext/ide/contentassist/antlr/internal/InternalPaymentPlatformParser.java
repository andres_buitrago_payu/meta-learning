package com.payulatam.paymentplatform.xtext.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import com.payulatam.paymentplatform.xtext.services.PaymentPlatformGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPaymentPlatformParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_COLON", "RULE_SQUARE_LEFT_BRACKET", "RULE_SQUARE_RIGHT_BRACKET", "RULE_COMMA", "RULE_LEFT_BRACKET", "RULE_RIGHT_BRACKET", "RULE_ARROW", "RULE_EQUAL", "RULE_SEMICOLON", "RULE_LEFT_PARENTHESIS", "RULE_RIGHT_PARENTHESIS", "RULE_DOT", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'In'", "'the'", "'adapter'", "'payment'", "'network'", "'uses'", "'check'", "'in'", "'url'", "'branch'", "'RestPaymentNetwork'", "'SOAPPaymentNetwork'"
    };
    public static final int RULE_SQUARE_LEFT_BRACKET=7;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=20;
    public static final int RULE_RIGHT_BRACKET=11;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int RULE_ARROW=12;
    public static final int RULE_LEFT_PARENTHESIS=15;
    public static final int RULE_DOT=17;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_SQUARE_RIGHT_BRACKET=8;
    public static final int T__32=32;
    public static final int RULE_RIGHT_PARENTHESIS=16;
    public static final int RULE_ID=5;
    public static final int RULE_EQUAL=13;
    public static final int RULE_COMMA=9;
    public static final int RULE_WS=21;
    public static final int RULE_COLON=6;
    public static final int RULE_LEFT_BRACKET=10;
    public static final int RULE_ANY_OTHER=22;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=18;
    public static final int T__29=29;
    public static final int RULE_ML_COMMENT=19;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_SEMICOLON=14;

    // delegates
    // delegators


        public InternalPaymentPlatformParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPaymentPlatformParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPaymentPlatformParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPaymentPlatform.g"; }


    	private PaymentPlatformGrammarAccess grammarAccess;

    	public void setGrammarAccess(PaymentPlatformGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRulePaymentPlaform"
    // InternalPaymentPlatform.g:53:1: entryRulePaymentPlaform : rulePaymentPlaform EOF ;
    public final void entryRulePaymentPlaform() throws RecognitionException {
        try {
            // InternalPaymentPlatform.g:54:1: ( rulePaymentPlaform EOF )
            // InternalPaymentPlatform.g:55:1: rulePaymentPlaform EOF
            {
             before(grammarAccess.getPaymentPlaformRule()); 
            pushFollow(FOLLOW_1);
            rulePaymentPlaform();

            state._fsp--;

             after(grammarAccess.getPaymentPlaformRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePaymentPlaform"


    // $ANTLR start "rulePaymentPlaform"
    // InternalPaymentPlatform.g:62:1: rulePaymentPlaform : ( ( rule__PaymentPlaform__Group__0 ) ) ;
    public final void rulePaymentPlaform() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:66:2: ( ( ( rule__PaymentPlaform__Group__0 ) ) )
            // InternalPaymentPlatform.g:67:2: ( ( rule__PaymentPlaform__Group__0 ) )
            {
            // InternalPaymentPlatform.g:67:2: ( ( rule__PaymentPlaform__Group__0 ) )
            // InternalPaymentPlatform.g:68:3: ( rule__PaymentPlaform__Group__0 )
            {
             before(grammarAccess.getPaymentPlaformAccess().getGroup()); 
            // InternalPaymentPlatform.g:69:3: ( rule__PaymentPlaform__Group__0 )
            // InternalPaymentPlatform.g:69:4: rule__PaymentPlaform__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPaymentPlaformAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePaymentPlaform"


    // $ANTLR start "entryRulePaymentNetwork"
    // InternalPaymentPlatform.g:78:1: entryRulePaymentNetwork : rulePaymentNetwork EOF ;
    public final void entryRulePaymentNetwork() throws RecognitionException {
        try {
            // InternalPaymentPlatform.g:79:1: ( rulePaymentNetwork EOF )
            // InternalPaymentPlatform.g:80:1: rulePaymentNetwork EOF
            {
             before(grammarAccess.getPaymentNetworkRule()); 
            pushFollow(FOLLOW_1);
            rulePaymentNetwork();

            state._fsp--;

             after(grammarAccess.getPaymentNetworkRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePaymentNetwork"


    // $ANTLR start "rulePaymentNetwork"
    // InternalPaymentPlatform.g:87:1: rulePaymentNetwork : ( ( rule__PaymentNetwork__Alternatives ) ) ;
    public final void rulePaymentNetwork() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:91:2: ( ( ( rule__PaymentNetwork__Alternatives ) ) )
            // InternalPaymentPlatform.g:92:2: ( ( rule__PaymentNetwork__Alternatives ) )
            {
            // InternalPaymentPlatform.g:92:2: ( ( rule__PaymentNetwork__Alternatives ) )
            // InternalPaymentPlatform.g:93:3: ( rule__PaymentNetwork__Alternatives )
            {
             before(grammarAccess.getPaymentNetworkAccess().getAlternatives()); 
            // InternalPaymentPlatform.g:94:3: ( rule__PaymentNetwork__Alternatives )
            // InternalPaymentPlatform.g:94:4: rule__PaymentNetwork__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__PaymentNetwork__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPaymentNetworkAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePaymentNetwork"


    // $ANTLR start "entryRuleEString"
    // InternalPaymentPlatform.g:103:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalPaymentPlatform.g:104:1: ( ruleEString EOF )
            // InternalPaymentPlatform.g:105:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalPaymentPlatform.g:112:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:116:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalPaymentPlatform.g:117:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalPaymentPlatform.g:117:2: ( ( rule__EString__Alternatives ) )
            // InternalPaymentPlatform.g:118:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalPaymentPlatform.g:119:3: ( rule__EString__Alternatives )
            // InternalPaymentPlatform.g:119:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleAdapter"
    // InternalPaymentPlatform.g:128:1: entryRuleAdapter : ruleAdapter EOF ;
    public final void entryRuleAdapter() throws RecognitionException {
        try {
            // InternalPaymentPlatform.g:129:1: ( ruleAdapter EOF )
            // InternalPaymentPlatform.g:130:1: ruleAdapter EOF
            {
             before(grammarAccess.getAdapterRule()); 
            pushFollow(FOLLOW_1);
            ruleAdapter();

            state._fsp--;

             after(grammarAccess.getAdapterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdapter"


    // $ANTLR start "ruleAdapter"
    // InternalPaymentPlatform.g:137:1: ruleAdapter : ( ( rule__Adapter__Group__0 ) ) ;
    public final void ruleAdapter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:141:2: ( ( ( rule__Adapter__Group__0 ) ) )
            // InternalPaymentPlatform.g:142:2: ( ( rule__Adapter__Group__0 ) )
            {
            // InternalPaymentPlatform.g:142:2: ( ( rule__Adapter__Group__0 ) )
            // InternalPaymentPlatform.g:143:3: ( rule__Adapter__Group__0 )
            {
             before(grammarAccess.getAdapterAccess().getGroup()); 
            // InternalPaymentPlatform.g:144:3: ( rule__Adapter__Group__0 )
            // InternalPaymentPlatform.g:144:4: rule__Adapter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Adapter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAdapterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdapter"


    // $ANTLR start "entryRuleRepository"
    // InternalPaymentPlatform.g:153:1: entryRuleRepository : ruleRepository EOF ;
    public final void entryRuleRepository() throws RecognitionException {
        try {
            // InternalPaymentPlatform.g:154:1: ( ruleRepository EOF )
            // InternalPaymentPlatform.g:155:1: ruleRepository EOF
            {
             before(grammarAccess.getRepositoryRule()); 
            pushFollow(FOLLOW_1);
            ruleRepository();

            state._fsp--;

             after(grammarAccess.getRepositoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRepository"


    // $ANTLR start "ruleRepository"
    // InternalPaymentPlatform.g:162:1: ruleRepository : ( ( rule__Repository__Group__0 ) ) ;
    public final void ruleRepository() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:166:2: ( ( ( rule__Repository__Group__0 ) ) )
            // InternalPaymentPlatform.g:167:2: ( ( rule__Repository__Group__0 ) )
            {
            // InternalPaymentPlatform.g:167:2: ( ( rule__Repository__Group__0 ) )
            // InternalPaymentPlatform.g:168:3: ( rule__Repository__Group__0 )
            {
             before(grammarAccess.getRepositoryAccess().getGroup()); 
            // InternalPaymentPlatform.g:169:3: ( rule__Repository__Group__0 )
            // InternalPaymentPlatform.g:169:4: rule__Repository__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Repository__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRepositoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRepository"


    // $ANTLR start "entryRuleRestPaymentNetwork"
    // InternalPaymentPlatform.g:178:1: entryRuleRestPaymentNetwork : ruleRestPaymentNetwork EOF ;
    public final void entryRuleRestPaymentNetwork() throws RecognitionException {
        try {
            // InternalPaymentPlatform.g:179:1: ( ruleRestPaymentNetwork EOF )
            // InternalPaymentPlatform.g:180:1: ruleRestPaymentNetwork EOF
            {
             before(grammarAccess.getRestPaymentNetworkRule()); 
            pushFollow(FOLLOW_1);
            ruleRestPaymentNetwork();

            state._fsp--;

             after(grammarAccess.getRestPaymentNetworkRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRestPaymentNetwork"


    // $ANTLR start "ruleRestPaymentNetwork"
    // InternalPaymentPlatform.g:187:1: ruleRestPaymentNetwork : ( ( rule__RestPaymentNetwork__Group__0 ) ) ;
    public final void ruleRestPaymentNetwork() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:191:2: ( ( ( rule__RestPaymentNetwork__Group__0 ) ) )
            // InternalPaymentPlatform.g:192:2: ( ( rule__RestPaymentNetwork__Group__0 ) )
            {
            // InternalPaymentPlatform.g:192:2: ( ( rule__RestPaymentNetwork__Group__0 ) )
            // InternalPaymentPlatform.g:193:3: ( rule__RestPaymentNetwork__Group__0 )
            {
             before(grammarAccess.getRestPaymentNetworkAccess().getGroup()); 
            // InternalPaymentPlatform.g:194:3: ( rule__RestPaymentNetwork__Group__0 )
            // InternalPaymentPlatform.g:194:4: rule__RestPaymentNetwork__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RestPaymentNetwork__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRestPaymentNetworkAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRestPaymentNetwork"


    // $ANTLR start "entryRuleSOAPPaymentNetwork"
    // InternalPaymentPlatform.g:203:1: entryRuleSOAPPaymentNetwork : ruleSOAPPaymentNetwork EOF ;
    public final void entryRuleSOAPPaymentNetwork() throws RecognitionException {
        try {
            // InternalPaymentPlatform.g:204:1: ( ruleSOAPPaymentNetwork EOF )
            // InternalPaymentPlatform.g:205:1: ruleSOAPPaymentNetwork EOF
            {
             before(grammarAccess.getSOAPPaymentNetworkRule()); 
            pushFollow(FOLLOW_1);
            ruleSOAPPaymentNetwork();

            state._fsp--;

             after(grammarAccess.getSOAPPaymentNetworkRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSOAPPaymentNetwork"


    // $ANTLR start "ruleSOAPPaymentNetwork"
    // InternalPaymentPlatform.g:212:1: ruleSOAPPaymentNetwork : ( ( rule__SOAPPaymentNetwork__Group__0 ) ) ;
    public final void ruleSOAPPaymentNetwork() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:216:2: ( ( ( rule__SOAPPaymentNetwork__Group__0 ) ) )
            // InternalPaymentPlatform.g:217:2: ( ( rule__SOAPPaymentNetwork__Group__0 ) )
            {
            // InternalPaymentPlatform.g:217:2: ( ( rule__SOAPPaymentNetwork__Group__0 ) )
            // InternalPaymentPlatform.g:218:3: ( rule__SOAPPaymentNetwork__Group__0 )
            {
             before(grammarAccess.getSOAPPaymentNetworkAccess().getGroup()); 
            // InternalPaymentPlatform.g:219:3: ( rule__SOAPPaymentNetwork__Group__0 )
            // InternalPaymentPlatform.g:219:4: rule__SOAPPaymentNetwork__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SOAPPaymentNetwork__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSOAPPaymentNetworkAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSOAPPaymentNetwork"


    // $ANTLR start "rule__PaymentNetwork__Alternatives"
    // InternalPaymentPlatform.g:227:1: rule__PaymentNetwork__Alternatives : ( ( ruleRestPaymentNetwork ) | ( ruleSOAPPaymentNetwork ) );
    public final void rule__PaymentNetwork__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:231:1: ( ( ruleRestPaymentNetwork ) | ( ruleSOAPPaymentNetwork ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==33) ) {
                alt1=1;
            }
            else if ( (LA1_0==34) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalPaymentPlatform.g:232:2: ( ruleRestPaymentNetwork )
                    {
                    // InternalPaymentPlatform.g:232:2: ( ruleRestPaymentNetwork )
                    // InternalPaymentPlatform.g:233:3: ruleRestPaymentNetwork
                    {
                     before(grammarAccess.getPaymentNetworkAccess().getRestPaymentNetworkParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleRestPaymentNetwork();

                    state._fsp--;

                     after(grammarAccess.getPaymentNetworkAccess().getRestPaymentNetworkParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPaymentPlatform.g:238:2: ( ruleSOAPPaymentNetwork )
                    {
                    // InternalPaymentPlatform.g:238:2: ( ruleSOAPPaymentNetwork )
                    // InternalPaymentPlatform.g:239:3: ruleSOAPPaymentNetwork
                    {
                     before(grammarAccess.getPaymentNetworkAccess().getSOAPPaymentNetworkParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleSOAPPaymentNetwork();

                    state._fsp--;

                     after(grammarAccess.getPaymentNetworkAccess().getSOAPPaymentNetworkParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentNetwork__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalPaymentPlatform.g:248:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:252:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalPaymentPlatform.g:253:2: ( RULE_STRING )
                    {
                    // InternalPaymentPlatform.g:253:2: ( RULE_STRING )
                    // InternalPaymentPlatform.g:254:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalPaymentPlatform.g:259:2: ( RULE_ID )
                    {
                    // InternalPaymentPlatform.g:259:2: ( RULE_ID )
                    // InternalPaymentPlatform.g:260:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__PaymentPlaform__Group__0"
    // InternalPaymentPlatform.g:269:1: rule__PaymentPlaform__Group__0 : rule__PaymentPlaform__Group__0__Impl rule__PaymentPlaform__Group__1 ;
    public final void rule__PaymentPlaform__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:273:1: ( rule__PaymentPlaform__Group__0__Impl rule__PaymentPlaform__Group__1 )
            // InternalPaymentPlatform.g:274:2: rule__PaymentPlaform__Group__0__Impl rule__PaymentPlaform__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__PaymentPlaform__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__0"


    // $ANTLR start "rule__PaymentPlaform__Group__0__Impl"
    // InternalPaymentPlatform.g:281:1: rule__PaymentPlaform__Group__0__Impl : ( () ) ;
    public final void rule__PaymentPlaform__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:285:1: ( ( () ) )
            // InternalPaymentPlatform.g:286:1: ( () )
            {
            // InternalPaymentPlatform.g:286:1: ( () )
            // InternalPaymentPlatform.g:287:2: ()
            {
             before(grammarAccess.getPaymentPlaformAccess().getPaymentPlaformAction_0()); 
            // InternalPaymentPlatform.g:288:2: ()
            // InternalPaymentPlatform.g:288:3: 
            {
            }

             after(grammarAccess.getPaymentPlaformAccess().getPaymentPlaformAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__0__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__1"
    // InternalPaymentPlatform.g:296:1: rule__PaymentPlaform__Group__1 : rule__PaymentPlaform__Group__1__Impl rule__PaymentPlaform__Group__2 ;
    public final void rule__PaymentPlaform__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:300:1: ( rule__PaymentPlaform__Group__1__Impl rule__PaymentPlaform__Group__2 )
            // InternalPaymentPlatform.g:301:2: rule__PaymentPlaform__Group__1__Impl rule__PaymentPlaform__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__PaymentPlaform__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__1"


    // $ANTLR start "rule__PaymentPlaform__Group__1__Impl"
    // InternalPaymentPlatform.g:308:1: rule__PaymentPlaform__Group__1__Impl : ( 'In' ) ;
    public final void rule__PaymentPlaform__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:312:1: ( ( 'In' ) )
            // InternalPaymentPlatform.g:313:1: ( 'In' )
            {
            // InternalPaymentPlatform.g:313:1: ( 'In' )
            // InternalPaymentPlatform.g:314:2: 'In'
            {
             before(grammarAccess.getPaymentPlaformAccess().getInKeyword_1()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getInKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__1__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__2"
    // InternalPaymentPlatform.g:323:1: rule__PaymentPlaform__Group__2 : rule__PaymentPlaform__Group__2__Impl rule__PaymentPlaform__Group__3 ;
    public final void rule__PaymentPlaform__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:327:1: ( rule__PaymentPlaform__Group__2__Impl rule__PaymentPlaform__Group__3 )
            // InternalPaymentPlatform.g:328:2: rule__PaymentPlaform__Group__2__Impl rule__PaymentPlaform__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__PaymentPlaform__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__2"


    // $ANTLR start "rule__PaymentPlaform__Group__2__Impl"
    // InternalPaymentPlatform.g:335:1: rule__PaymentPlaform__Group__2__Impl : ( ( rule__PaymentPlaform__NameAssignment_2 ) ) ;
    public final void rule__PaymentPlaform__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:339:1: ( ( ( rule__PaymentPlaform__NameAssignment_2 ) ) )
            // InternalPaymentPlatform.g:340:1: ( ( rule__PaymentPlaform__NameAssignment_2 ) )
            {
            // InternalPaymentPlatform.g:340:1: ( ( rule__PaymentPlaform__NameAssignment_2 ) )
            // InternalPaymentPlatform.g:341:2: ( rule__PaymentPlaform__NameAssignment_2 )
            {
             before(grammarAccess.getPaymentPlaformAccess().getNameAssignment_2()); 
            // InternalPaymentPlatform.g:342:2: ( rule__PaymentPlaform__NameAssignment_2 )
            // InternalPaymentPlatform.g:342:3: rule__PaymentPlaform__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPaymentPlaformAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__2__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__3"
    // InternalPaymentPlatform.g:350:1: rule__PaymentPlaform__Group__3 : rule__PaymentPlaform__Group__3__Impl rule__PaymentPlaform__Group__4 ;
    public final void rule__PaymentPlaform__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:354:1: ( rule__PaymentPlaform__Group__3__Impl rule__PaymentPlaform__Group__4 )
            // InternalPaymentPlatform.g:355:2: rule__PaymentPlaform__Group__3__Impl rule__PaymentPlaform__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__PaymentPlaform__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__3"


    // $ANTLR start "rule__PaymentPlaform__Group__3__Impl"
    // InternalPaymentPlatform.g:362:1: rule__PaymentPlaform__Group__3__Impl : ( RULE_COLON ) ;
    public final void rule__PaymentPlaform__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:366:1: ( ( RULE_COLON ) )
            // InternalPaymentPlatform.g:367:1: ( RULE_COLON )
            {
            // InternalPaymentPlatform.g:367:1: ( RULE_COLON )
            // InternalPaymentPlatform.g:368:2: RULE_COLON
            {
             before(grammarAccess.getPaymentPlaformAccess().getCOLONTerminalRuleCall_3()); 
            match(input,RULE_COLON,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getCOLONTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__3__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__4"
    // InternalPaymentPlatform.g:377:1: rule__PaymentPlaform__Group__4 : rule__PaymentPlaform__Group__4__Impl rule__PaymentPlaform__Group__5 ;
    public final void rule__PaymentPlaform__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:381:1: ( rule__PaymentPlaform__Group__4__Impl rule__PaymentPlaform__Group__5 )
            // InternalPaymentPlatform.g:382:2: rule__PaymentPlaform__Group__4__Impl rule__PaymentPlaform__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__PaymentPlaform__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__4"


    // $ANTLR start "rule__PaymentPlaform__Group__4__Impl"
    // InternalPaymentPlatform.g:389:1: rule__PaymentPlaform__Group__4__Impl : ( 'the' ) ;
    public final void rule__PaymentPlaform__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:393:1: ( ( 'the' ) )
            // InternalPaymentPlatform.g:394:1: ( 'the' )
            {
            // InternalPaymentPlatform.g:394:1: ( 'the' )
            // InternalPaymentPlatform.g:395:2: 'the'
            {
             before(grammarAccess.getPaymentPlaformAccess().getTheKeyword_4()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getTheKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__4__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__5"
    // InternalPaymentPlatform.g:404:1: rule__PaymentPlaform__Group__5 : rule__PaymentPlaform__Group__5__Impl rule__PaymentPlaform__Group__6 ;
    public final void rule__PaymentPlaform__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:408:1: ( rule__PaymentPlaform__Group__5__Impl rule__PaymentPlaform__Group__6 )
            // InternalPaymentPlatform.g:409:2: rule__PaymentPlaform__Group__5__Impl rule__PaymentPlaform__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__PaymentPlaform__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__5"


    // $ANTLR start "rule__PaymentPlaform__Group__5__Impl"
    // InternalPaymentPlatform.g:416:1: rule__PaymentPlaform__Group__5__Impl : ( 'adapter' ) ;
    public final void rule__PaymentPlaform__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:420:1: ( ( 'adapter' ) )
            // InternalPaymentPlatform.g:421:1: ( 'adapter' )
            {
            // InternalPaymentPlatform.g:421:1: ( 'adapter' )
            // InternalPaymentPlatform.g:422:2: 'adapter'
            {
             before(grammarAccess.getPaymentPlaformAccess().getAdapterKeyword_5()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getAdapterKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__5__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__6"
    // InternalPaymentPlatform.g:431:1: rule__PaymentPlaform__Group__6 : rule__PaymentPlaform__Group__6__Impl rule__PaymentPlaform__Group__7 ;
    public final void rule__PaymentPlaform__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:435:1: ( rule__PaymentPlaform__Group__6__Impl rule__PaymentPlaform__Group__7 )
            // InternalPaymentPlatform.g:436:2: rule__PaymentPlaform__Group__6__Impl rule__PaymentPlaform__Group__7
            {
            pushFollow(FOLLOW_4);
            rule__PaymentPlaform__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__6"


    // $ANTLR start "rule__PaymentPlaform__Group__6__Impl"
    // InternalPaymentPlatform.g:443:1: rule__PaymentPlaform__Group__6__Impl : ( RULE_SQUARE_LEFT_BRACKET ) ;
    public final void rule__PaymentPlaform__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:447:1: ( ( RULE_SQUARE_LEFT_BRACKET ) )
            // InternalPaymentPlatform.g:448:1: ( RULE_SQUARE_LEFT_BRACKET )
            {
            // InternalPaymentPlatform.g:448:1: ( RULE_SQUARE_LEFT_BRACKET )
            // InternalPaymentPlatform.g:449:2: RULE_SQUARE_LEFT_BRACKET
            {
             before(grammarAccess.getPaymentPlaformAccess().getSQUARE_LEFT_BRACKETTerminalRuleCall_6()); 
            match(input,RULE_SQUARE_LEFT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getSQUARE_LEFT_BRACKETTerminalRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__6__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__7"
    // InternalPaymentPlatform.g:458:1: rule__PaymentPlaform__Group__7 : rule__PaymentPlaform__Group__7__Impl rule__PaymentPlaform__Group__8 ;
    public final void rule__PaymentPlaform__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:462:1: ( rule__PaymentPlaform__Group__7__Impl rule__PaymentPlaform__Group__8 )
            // InternalPaymentPlatform.g:463:2: rule__PaymentPlaform__Group__7__Impl rule__PaymentPlaform__Group__8
            {
            pushFollow(FOLLOW_9);
            rule__PaymentPlaform__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__7"


    // $ANTLR start "rule__PaymentPlaform__Group__7__Impl"
    // InternalPaymentPlatform.g:470:1: rule__PaymentPlaform__Group__7__Impl : ( ( rule__PaymentPlaform__AdapterAssignment_7 ) ) ;
    public final void rule__PaymentPlaform__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:474:1: ( ( ( rule__PaymentPlaform__AdapterAssignment_7 ) ) )
            // InternalPaymentPlatform.g:475:1: ( ( rule__PaymentPlaform__AdapterAssignment_7 ) )
            {
            // InternalPaymentPlatform.g:475:1: ( ( rule__PaymentPlaform__AdapterAssignment_7 ) )
            // InternalPaymentPlatform.g:476:2: ( rule__PaymentPlaform__AdapterAssignment_7 )
            {
             before(grammarAccess.getPaymentPlaformAccess().getAdapterAssignment_7()); 
            // InternalPaymentPlatform.g:477:2: ( rule__PaymentPlaform__AdapterAssignment_7 )
            // InternalPaymentPlatform.g:477:3: rule__PaymentPlaform__AdapterAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__AdapterAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getPaymentPlaformAccess().getAdapterAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__7__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__8"
    // InternalPaymentPlatform.g:485:1: rule__PaymentPlaform__Group__8 : rule__PaymentPlaform__Group__8__Impl rule__PaymentPlaform__Group__9 ;
    public final void rule__PaymentPlaform__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:489:1: ( rule__PaymentPlaform__Group__8__Impl rule__PaymentPlaform__Group__9 )
            // InternalPaymentPlatform.g:490:2: rule__PaymentPlaform__Group__8__Impl rule__PaymentPlaform__Group__9
            {
            pushFollow(FOLLOW_9);
            rule__PaymentPlaform__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__8"


    // $ANTLR start "rule__PaymentPlaform__Group__8__Impl"
    // InternalPaymentPlatform.g:497:1: rule__PaymentPlaform__Group__8__Impl : ( ( rule__PaymentPlaform__Group_8__0 )* ) ;
    public final void rule__PaymentPlaform__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:501:1: ( ( ( rule__PaymentPlaform__Group_8__0 )* ) )
            // InternalPaymentPlatform.g:502:1: ( ( rule__PaymentPlaform__Group_8__0 )* )
            {
            // InternalPaymentPlatform.g:502:1: ( ( rule__PaymentPlaform__Group_8__0 )* )
            // InternalPaymentPlatform.g:503:2: ( rule__PaymentPlaform__Group_8__0 )*
            {
             before(grammarAccess.getPaymentPlaformAccess().getGroup_8()); 
            // InternalPaymentPlatform.g:504:2: ( rule__PaymentPlaform__Group_8__0 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_COMMA) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalPaymentPlatform.g:504:3: rule__PaymentPlaform__Group_8__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__PaymentPlaform__Group_8__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getPaymentPlaformAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__8__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__9"
    // InternalPaymentPlatform.g:512:1: rule__PaymentPlaform__Group__9 : rule__PaymentPlaform__Group__9__Impl rule__PaymentPlaform__Group__10 ;
    public final void rule__PaymentPlaform__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:516:1: ( rule__PaymentPlaform__Group__9__Impl rule__PaymentPlaform__Group__10 )
            // InternalPaymentPlatform.g:517:2: rule__PaymentPlaform__Group__9__Impl rule__PaymentPlaform__Group__10
            {
            pushFollow(FOLLOW_6);
            rule__PaymentPlaform__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__9"


    // $ANTLR start "rule__PaymentPlaform__Group__9__Impl"
    // InternalPaymentPlatform.g:524:1: rule__PaymentPlaform__Group__9__Impl : ( RULE_SQUARE_RIGHT_BRACKET ) ;
    public final void rule__PaymentPlaform__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:528:1: ( ( RULE_SQUARE_RIGHT_BRACKET ) )
            // InternalPaymentPlatform.g:529:1: ( RULE_SQUARE_RIGHT_BRACKET )
            {
            // InternalPaymentPlatform.g:529:1: ( RULE_SQUARE_RIGHT_BRACKET )
            // InternalPaymentPlatform.g:530:2: RULE_SQUARE_RIGHT_BRACKET
            {
             before(grammarAccess.getPaymentPlaformAccess().getSQUARE_RIGHT_BRACKETTerminalRuleCall_9()); 
            match(input,RULE_SQUARE_RIGHT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getSQUARE_RIGHT_BRACKETTerminalRuleCall_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__9__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__10"
    // InternalPaymentPlatform.g:539:1: rule__PaymentPlaform__Group__10 : rule__PaymentPlaform__Group__10__Impl rule__PaymentPlaform__Group__11 ;
    public final void rule__PaymentPlaform__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:543:1: ( rule__PaymentPlaform__Group__10__Impl rule__PaymentPlaform__Group__11 )
            // InternalPaymentPlatform.g:544:2: rule__PaymentPlaform__Group__10__Impl rule__PaymentPlaform__Group__11
            {
            pushFollow(FOLLOW_11);
            rule__PaymentPlaform__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__10"


    // $ANTLR start "rule__PaymentPlaform__Group__10__Impl"
    // InternalPaymentPlatform.g:551:1: rule__PaymentPlaform__Group__10__Impl : ( 'the' ) ;
    public final void rule__PaymentPlaform__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:555:1: ( ( 'the' ) )
            // InternalPaymentPlatform.g:556:1: ( 'the' )
            {
            // InternalPaymentPlatform.g:556:1: ( 'the' )
            // InternalPaymentPlatform.g:557:2: 'the'
            {
             before(grammarAccess.getPaymentPlaformAccess().getTheKeyword_10()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getTheKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__10__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__11"
    // InternalPaymentPlatform.g:566:1: rule__PaymentPlaform__Group__11 : rule__PaymentPlaform__Group__11__Impl rule__PaymentPlaform__Group__12 ;
    public final void rule__PaymentPlaform__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:570:1: ( rule__PaymentPlaform__Group__11__Impl rule__PaymentPlaform__Group__12 )
            // InternalPaymentPlatform.g:571:2: rule__PaymentPlaform__Group__11__Impl rule__PaymentPlaform__Group__12
            {
            pushFollow(FOLLOW_12);
            rule__PaymentPlaform__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__11"


    // $ANTLR start "rule__PaymentPlaform__Group__11__Impl"
    // InternalPaymentPlatform.g:578:1: rule__PaymentPlaform__Group__11__Impl : ( 'payment' ) ;
    public final void rule__PaymentPlaform__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:582:1: ( ( 'payment' ) )
            // InternalPaymentPlatform.g:583:1: ( 'payment' )
            {
            // InternalPaymentPlatform.g:583:1: ( 'payment' )
            // InternalPaymentPlatform.g:584:2: 'payment'
            {
             before(grammarAccess.getPaymentPlaformAccess().getPaymentKeyword_11()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getPaymentKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__11__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__12"
    // InternalPaymentPlatform.g:593:1: rule__PaymentPlaform__Group__12 : rule__PaymentPlaform__Group__12__Impl rule__PaymentPlaform__Group__13 ;
    public final void rule__PaymentPlaform__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:597:1: ( rule__PaymentPlaform__Group__12__Impl rule__PaymentPlaform__Group__13 )
            // InternalPaymentPlatform.g:598:2: rule__PaymentPlaform__Group__12__Impl rule__PaymentPlaform__Group__13
            {
            pushFollow(FOLLOW_8);
            rule__PaymentPlaform__Group__12__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__12"


    // $ANTLR start "rule__PaymentPlaform__Group__12__Impl"
    // InternalPaymentPlatform.g:605:1: rule__PaymentPlaform__Group__12__Impl : ( 'network' ) ;
    public final void rule__PaymentPlaform__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:609:1: ( ( 'network' ) )
            // InternalPaymentPlatform.g:610:1: ( 'network' )
            {
            // InternalPaymentPlatform.g:610:1: ( 'network' )
            // InternalPaymentPlatform.g:611:2: 'network'
            {
             before(grammarAccess.getPaymentPlaformAccess().getNetworkKeyword_12()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getNetworkKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__12__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__13"
    // InternalPaymentPlatform.g:620:1: rule__PaymentPlaform__Group__13 : rule__PaymentPlaform__Group__13__Impl rule__PaymentPlaform__Group__14 ;
    public final void rule__PaymentPlaform__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:624:1: ( rule__PaymentPlaform__Group__13__Impl rule__PaymentPlaform__Group__14 )
            // InternalPaymentPlatform.g:625:2: rule__PaymentPlaform__Group__13__Impl rule__PaymentPlaform__Group__14
            {
            pushFollow(FOLLOW_13);
            rule__PaymentPlaform__Group__13__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__13"


    // $ANTLR start "rule__PaymentPlaform__Group__13__Impl"
    // InternalPaymentPlatform.g:632:1: rule__PaymentPlaform__Group__13__Impl : ( RULE_SQUARE_LEFT_BRACKET ) ;
    public final void rule__PaymentPlaform__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:636:1: ( ( RULE_SQUARE_LEFT_BRACKET ) )
            // InternalPaymentPlatform.g:637:1: ( RULE_SQUARE_LEFT_BRACKET )
            {
            // InternalPaymentPlatform.g:637:1: ( RULE_SQUARE_LEFT_BRACKET )
            // InternalPaymentPlatform.g:638:2: RULE_SQUARE_LEFT_BRACKET
            {
             before(grammarAccess.getPaymentPlaformAccess().getSQUARE_LEFT_BRACKETTerminalRuleCall_13()); 
            match(input,RULE_SQUARE_LEFT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getSQUARE_LEFT_BRACKETTerminalRuleCall_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__13__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__14"
    // InternalPaymentPlatform.g:647:1: rule__PaymentPlaform__Group__14 : rule__PaymentPlaform__Group__14__Impl rule__PaymentPlaform__Group__15 ;
    public final void rule__PaymentPlaform__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:651:1: ( rule__PaymentPlaform__Group__14__Impl rule__PaymentPlaform__Group__15 )
            // InternalPaymentPlatform.g:652:2: rule__PaymentPlaform__Group__14__Impl rule__PaymentPlaform__Group__15
            {
            pushFollow(FOLLOW_9);
            rule__PaymentPlaform__Group__14__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__14"


    // $ANTLR start "rule__PaymentPlaform__Group__14__Impl"
    // InternalPaymentPlatform.g:659:1: rule__PaymentPlaform__Group__14__Impl : ( ( rule__PaymentPlaform__PaymentnetworkAssignment_14 ) ) ;
    public final void rule__PaymentPlaform__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:663:1: ( ( ( rule__PaymentPlaform__PaymentnetworkAssignment_14 ) ) )
            // InternalPaymentPlatform.g:664:1: ( ( rule__PaymentPlaform__PaymentnetworkAssignment_14 ) )
            {
            // InternalPaymentPlatform.g:664:1: ( ( rule__PaymentPlaform__PaymentnetworkAssignment_14 ) )
            // InternalPaymentPlatform.g:665:2: ( rule__PaymentPlaform__PaymentnetworkAssignment_14 )
            {
             before(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkAssignment_14()); 
            // InternalPaymentPlatform.g:666:2: ( rule__PaymentPlaform__PaymentnetworkAssignment_14 )
            // InternalPaymentPlatform.g:666:3: rule__PaymentPlaform__PaymentnetworkAssignment_14
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__PaymentnetworkAssignment_14();

            state._fsp--;


            }

             after(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkAssignment_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__14__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__15"
    // InternalPaymentPlatform.g:674:1: rule__PaymentPlaform__Group__15 : rule__PaymentPlaform__Group__15__Impl rule__PaymentPlaform__Group__16 ;
    public final void rule__PaymentPlaform__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:678:1: ( rule__PaymentPlaform__Group__15__Impl rule__PaymentPlaform__Group__16 )
            // InternalPaymentPlatform.g:679:2: rule__PaymentPlaform__Group__15__Impl rule__PaymentPlaform__Group__16
            {
            pushFollow(FOLLOW_9);
            rule__PaymentPlaform__Group__15__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__15"


    // $ANTLR start "rule__PaymentPlaform__Group__15__Impl"
    // InternalPaymentPlatform.g:686:1: rule__PaymentPlaform__Group__15__Impl : ( ( rule__PaymentPlaform__Group_15__0 )* ) ;
    public final void rule__PaymentPlaform__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:690:1: ( ( ( rule__PaymentPlaform__Group_15__0 )* ) )
            // InternalPaymentPlatform.g:691:1: ( ( rule__PaymentPlaform__Group_15__0 )* )
            {
            // InternalPaymentPlatform.g:691:1: ( ( rule__PaymentPlaform__Group_15__0 )* )
            // InternalPaymentPlatform.g:692:2: ( rule__PaymentPlaform__Group_15__0 )*
            {
             before(grammarAccess.getPaymentPlaformAccess().getGroup_15()); 
            // InternalPaymentPlatform.g:693:2: ( rule__PaymentPlaform__Group_15__0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_COMMA) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalPaymentPlatform.g:693:3: rule__PaymentPlaform__Group_15__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__PaymentPlaform__Group_15__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getPaymentPlaformAccess().getGroup_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__15__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group__16"
    // InternalPaymentPlatform.g:701:1: rule__PaymentPlaform__Group__16 : rule__PaymentPlaform__Group__16__Impl ;
    public final void rule__PaymentPlaform__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:705:1: ( rule__PaymentPlaform__Group__16__Impl )
            // InternalPaymentPlatform.g:706:2: rule__PaymentPlaform__Group__16__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group__16__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__16"


    // $ANTLR start "rule__PaymentPlaform__Group__16__Impl"
    // InternalPaymentPlatform.g:712:1: rule__PaymentPlaform__Group__16__Impl : ( RULE_SQUARE_RIGHT_BRACKET ) ;
    public final void rule__PaymentPlaform__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:716:1: ( ( RULE_SQUARE_RIGHT_BRACKET ) )
            // InternalPaymentPlatform.g:717:1: ( RULE_SQUARE_RIGHT_BRACKET )
            {
            // InternalPaymentPlatform.g:717:1: ( RULE_SQUARE_RIGHT_BRACKET )
            // InternalPaymentPlatform.g:718:2: RULE_SQUARE_RIGHT_BRACKET
            {
             before(grammarAccess.getPaymentPlaformAccess().getSQUARE_RIGHT_BRACKETTerminalRuleCall_16()); 
            match(input,RULE_SQUARE_RIGHT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getSQUARE_RIGHT_BRACKETTerminalRuleCall_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group__16__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group_8__0"
    // InternalPaymentPlatform.g:728:1: rule__PaymentPlaform__Group_8__0 : rule__PaymentPlaform__Group_8__0__Impl rule__PaymentPlaform__Group_8__1 ;
    public final void rule__PaymentPlaform__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:732:1: ( rule__PaymentPlaform__Group_8__0__Impl rule__PaymentPlaform__Group_8__1 )
            // InternalPaymentPlatform.g:733:2: rule__PaymentPlaform__Group_8__0__Impl rule__PaymentPlaform__Group_8__1
            {
            pushFollow(FOLLOW_4);
            rule__PaymentPlaform__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_8__0"


    // $ANTLR start "rule__PaymentPlaform__Group_8__0__Impl"
    // InternalPaymentPlatform.g:740:1: rule__PaymentPlaform__Group_8__0__Impl : ( RULE_COMMA ) ;
    public final void rule__PaymentPlaform__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:744:1: ( ( RULE_COMMA ) )
            // InternalPaymentPlatform.g:745:1: ( RULE_COMMA )
            {
            // InternalPaymentPlatform.g:745:1: ( RULE_COMMA )
            // InternalPaymentPlatform.g:746:2: RULE_COMMA
            {
             before(grammarAccess.getPaymentPlaformAccess().getCOMMATerminalRuleCall_8_0()); 
            match(input,RULE_COMMA,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getCOMMATerminalRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_8__0__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group_8__1"
    // InternalPaymentPlatform.g:755:1: rule__PaymentPlaform__Group_8__1 : rule__PaymentPlaform__Group_8__1__Impl ;
    public final void rule__PaymentPlaform__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:759:1: ( rule__PaymentPlaform__Group_8__1__Impl )
            // InternalPaymentPlatform.g:760:2: rule__PaymentPlaform__Group_8__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group_8__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_8__1"


    // $ANTLR start "rule__PaymentPlaform__Group_8__1__Impl"
    // InternalPaymentPlatform.g:766:1: rule__PaymentPlaform__Group_8__1__Impl : ( ( rule__PaymentPlaform__AdapterAssignment_8_1 ) ) ;
    public final void rule__PaymentPlaform__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:770:1: ( ( ( rule__PaymentPlaform__AdapterAssignment_8_1 ) ) )
            // InternalPaymentPlatform.g:771:1: ( ( rule__PaymentPlaform__AdapterAssignment_8_1 ) )
            {
            // InternalPaymentPlatform.g:771:1: ( ( rule__PaymentPlaform__AdapterAssignment_8_1 ) )
            // InternalPaymentPlatform.g:772:2: ( rule__PaymentPlaform__AdapterAssignment_8_1 )
            {
             before(grammarAccess.getPaymentPlaformAccess().getAdapterAssignment_8_1()); 
            // InternalPaymentPlatform.g:773:2: ( rule__PaymentPlaform__AdapterAssignment_8_1 )
            // InternalPaymentPlatform.g:773:3: rule__PaymentPlaform__AdapterAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__AdapterAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getPaymentPlaformAccess().getAdapterAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_8__1__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group_15__0"
    // InternalPaymentPlatform.g:782:1: rule__PaymentPlaform__Group_15__0 : rule__PaymentPlaform__Group_15__0__Impl rule__PaymentPlaform__Group_15__1 ;
    public final void rule__PaymentPlaform__Group_15__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:786:1: ( rule__PaymentPlaform__Group_15__0__Impl rule__PaymentPlaform__Group_15__1 )
            // InternalPaymentPlatform.g:787:2: rule__PaymentPlaform__Group_15__0__Impl rule__PaymentPlaform__Group_15__1
            {
            pushFollow(FOLLOW_13);
            rule__PaymentPlaform__Group_15__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group_15__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_15__0"


    // $ANTLR start "rule__PaymentPlaform__Group_15__0__Impl"
    // InternalPaymentPlatform.g:794:1: rule__PaymentPlaform__Group_15__0__Impl : ( RULE_COMMA ) ;
    public final void rule__PaymentPlaform__Group_15__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:798:1: ( ( RULE_COMMA ) )
            // InternalPaymentPlatform.g:799:1: ( RULE_COMMA )
            {
            // InternalPaymentPlatform.g:799:1: ( RULE_COMMA )
            // InternalPaymentPlatform.g:800:2: RULE_COMMA
            {
             before(grammarAccess.getPaymentPlaformAccess().getCOMMATerminalRuleCall_15_0()); 
            match(input,RULE_COMMA,FOLLOW_2); 
             after(grammarAccess.getPaymentPlaformAccess().getCOMMATerminalRuleCall_15_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_15__0__Impl"


    // $ANTLR start "rule__PaymentPlaform__Group_15__1"
    // InternalPaymentPlatform.g:809:1: rule__PaymentPlaform__Group_15__1 : rule__PaymentPlaform__Group_15__1__Impl ;
    public final void rule__PaymentPlaform__Group_15__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:813:1: ( rule__PaymentPlaform__Group_15__1__Impl )
            // InternalPaymentPlatform.g:814:2: rule__PaymentPlaform__Group_15__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__Group_15__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_15__1"


    // $ANTLR start "rule__PaymentPlaform__Group_15__1__Impl"
    // InternalPaymentPlatform.g:820:1: rule__PaymentPlaform__Group_15__1__Impl : ( ( rule__PaymentPlaform__PaymentnetworkAssignment_15_1 ) ) ;
    public final void rule__PaymentPlaform__Group_15__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:824:1: ( ( ( rule__PaymentPlaform__PaymentnetworkAssignment_15_1 ) ) )
            // InternalPaymentPlatform.g:825:1: ( ( rule__PaymentPlaform__PaymentnetworkAssignment_15_1 ) )
            {
            // InternalPaymentPlatform.g:825:1: ( ( rule__PaymentPlaform__PaymentnetworkAssignment_15_1 ) )
            // InternalPaymentPlatform.g:826:2: ( rule__PaymentPlaform__PaymentnetworkAssignment_15_1 )
            {
             before(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkAssignment_15_1()); 
            // InternalPaymentPlatform.g:827:2: ( rule__PaymentPlaform__PaymentnetworkAssignment_15_1 )
            // InternalPaymentPlatform.g:827:3: rule__PaymentPlaform__PaymentnetworkAssignment_15_1
            {
            pushFollow(FOLLOW_2);
            rule__PaymentPlaform__PaymentnetworkAssignment_15_1();

            state._fsp--;


            }

             after(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkAssignment_15_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__Group_15__1__Impl"


    // $ANTLR start "rule__Adapter__Group__0"
    // InternalPaymentPlatform.g:836:1: rule__Adapter__Group__0 : rule__Adapter__Group__0__Impl rule__Adapter__Group__1 ;
    public final void rule__Adapter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:840:1: ( rule__Adapter__Group__0__Impl rule__Adapter__Group__1 )
            // InternalPaymentPlatform.g:841:2: rule__Adapter__Group__0__Impl rule__Adapter__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Adapter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Adapter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__0"


    // $ANTLR start "rule__Adapter__Group__0__Impl"
    // InternalPaymentPlatform.g:848:1: rule__Adapter__Group__0__Impl : ( ( rule__Adapter__NameAssignment_0 ) ) ;
    public final void rule__Adapter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:852:1: ( ( ( rule__Adapter__NameAssignment_0 ) ) )
            // InternalPaymentPlatform.g:853:1: ( ( rule__Adapter__NameAssignment_0 ) )
            {
            // InternalPaymentPlatform.g:853:1: ( ( rule__Adapter__NameAssignment_0 ) )
            // InternalPaymentPlatform.g:854:2: ( rule__Adapter__NameAssignment_0 )
            {
             before(grammarAccess.getAdapterAccess().getNameAssignment_0()); 
            // InternalPaymentPlatform.g:855:2: ( rule__Adapter__NameAssignment_0 )
            // InternalPaymentPlatform.g:855:3: rule__Adapter__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Adapter__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAdapterAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__0__Impl"


    // $ANTLR start "rule__Adapter__Group__1"
    // InternalPaymentPlatform.g:863:1: rule__Adapter__Group__1 : rule__Adapter__Group__1__Impl rule__Adapter__Group__2 ;
    public final void rule__Adapter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:867:1: ( rule__Adapter__Group__1__Impl rule__Adapter__Group__2 )
            // InternalPaymentPlatform.g:868:2: rule__Adapter__Group__1__Impl rule__Adapter__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__Adapter__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Adapter__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__1"


    // $ANTLR start "rule__Adapter__Group__1__Impl"
    // InternalPaymentPlatform.g:875:1: rule__Adapter__Group__1__Impl : ( RULE_LEFT_BRACKET ) ;
    public final void rule__Adapter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:879:1: ( ( RULE_LEFT_BRACKET ) )
            // InternalPaymentPlatform.g:880:1: ( RULE_LEFT_BRACKET )
            {
            // InternalPaymentPlatform.g:880:1: ( RULE_LEFT_BRACKET )
            // InternalPaymentPlatform.g:881:2: RULE_LEFT_BRACKET
            {
             before(grammarAccess.getAdapterAccess().getLEFT_BRACKETTerminalRuleCall_1()); 
            match(input,RULE_LEFT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getAdapterAccess().getLEFT_BRACKETTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__1__Impl"


    // $ANTLR start "rule__Adapter__Group__2"
    // InternalPaymentPlatform.g:890:1: rule__Adapter__Group__2 : rule__Adapter__Group__2__Impl rule__Adapter__Group__3 ;
    public final void rule__Adapter__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:894:1: ( rule__Adapter__Group__2__Impl rule__Adapter__Group__3 )
            // InternalPaymentPlatform.g:895:2: rule__Adapter__Group__2__Impl rule__Adapter__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Adapter__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Adapter__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__2"


    // $ANTLR start "rule__Adapter__Group__2__Impl"
    // InternalPaymentPlatform.g:902:1: rule__Adapter__Group__2__Impl : ( 'uses' ) ;
    public final void rule__Adapter__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:906:1: ( ( 'uses' ) )
            // InternalPaymentPlatform.g:907:1: ( 'uses' )
            {
            // InternalPaymentPlatform.g:907:1: ( 'uses' )
            // InternalPaymentPlatform.g:908:2: 'uses'
            {
             before(grammarAccess.getAdapterAccess().getUsesKeyword_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getAdapterAccess().getUsesKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__2__Impl"


    // $ANTLR start "rule__Adapter__Group__3"
    // InternalPaymentPlatform.g:917:1: rule__Adapter__Group__3 : rule__Adapter__Group__3__Impl rule__Adapter__Group__4 ;
    public final void rule__Adapter__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:921:1: ( rule__Adapter__Group__3__Impl rule__Adapter__Group__4 )
            // InternalPaymentPlatform.g:922:2: rule__Adapter__Group__3__Impl rule__Adapter__Group__4
            {
            pushFollow(FOLLOW_16);
            rule__Adapter__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Adapter__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__3"


    // $ANTLR start "rule__Adapter__Group__3__Impl"
    // InternalPaymentPlatform.g:929:1: rule__Adapter__Group__3__Impl : ( ( rule__Adapter__PaymentnetworkAssignment_3 ) ) ;
    public final void rule__Adapter__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:933:1: ( ( ( rule__Adapter__PaymentnetworkAssignment_3 ) ) )
            // InternalPaymentPlatform.g:934:1: ( ( rule__Adapter__PaymentnetworkAssignment_3 ) )
            {
            // InternalPaymentPlatform.g:934:1: ( ( rule__Adapter__PaymentnetworkAssignment_3 ) )
            // InternalPaymentPlatform.g:935:2: ( rule__Adapter__PaymentnetworkAssignment_3 )
            {
             before(grammarAccess.getAdapterAccess().getPaymentnetworkAssignment_3()); 
            // InternalPaymentPlatform.g:936:2: ( rule__Adapter__PaymentnetworkAssignment_3 )
            // InternalPaymentPlatform.g:936:3: rule__Adapter__PaymentnetworkAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Adapter__PaymentnetworkAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAdapterAccess().getPaymentnetworkAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__3__Impl"


    // $ANTLR start "rule__Adapter__Group__4"
    // InternalPaymentPlatform.g:944:1: rule__Adapter__Group__4 : rule__Adapter__Group__4__Impl rule__Adapter__Group__5 ;
    public final void rule__Adapter__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:948:1: ( rule__Adapter__Group__4__Impl rule__Adapter__Group__5 )
            // InternalPaymentPlatform.g:949:2: rule__Adapter__Group__4__Impl rule__Adapter__Group__5
            {
            pushFollow(FOLLOW_17);
            rule__Adapter__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Adapter__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__4"


    // $ANTLR start "rule__Adapter__Group__4__Impl"
    // InternalPaymentPlatform.g:956:1: rule__Adapter__Group__4__Impl : ( 'check' ) ;
    public final void rule__Adapter__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:960:1: ( ( 'check' ) )
            // InternalPaymentPlatform.g:961:1: ( 'check' )
            {
            // InternalPaymentPlatform.g:961:1: ( 'check' )
            // InternalPaymentPlatform.g:962:2: 'check'
            {
             before(grammarAccess.getAdapterAccess().getCheckKeyword_4()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getAdapterAccess().getCheckKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__4__Impl"


    // $ANTLR start "rule__Adapter__Group__5"
    // InternalPaymentPlatform.g:971:1: rule__Adapter__Group__5 : rule__Adapter__Group__5__Impl rule__Adapter__Group__6 ;
    public final void rule__Adapter__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:975:1: ( rule__Adapter__Group__5__Impl rule__Adapter__Group__6 )
            // InternalPaymentPlatform.g:976:2: rule__Adapter__Group__5__Impl rule__Adapter__Group__6
            {
            pushFollow(FOLLOW_14);
            rule__Adapter__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Adapter__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__5"


    // $ANTLR start "rule__Adapter__Group__5__Impl"
    // InternalPaymentPlatform.g:983:1: rule__Adapter__Group__5__Impl : ( 'in' ) ;
    public final void rule__Adapter__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:987:1: ( ( 'in' ) )
            // InternalPaymentPlatform.g:988:1: ( 'in' )
            {
            // InternalPaymentPlatform.g:988:1: ( 'in' )
            // InternalPaymentPlatform.g:989:2: 'in'
            {
             before(grammarAccess.getAdapterAccess().getInKeyword_5()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getAdapterAccess().getInKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__5__Impl"


    // $ANTLR start "rule__Adapter__Group__6"
    // InternalPaymentPlatform.g:998:1: rule__Adapter__Group__6 : rule__Adapter__Group__6__Impl rule__Adapter__Group__7 ;
    public final void rule__Adapter__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1002:1: ( rule__Adapter__Group__6__Impl rule__Adapter__Group__7 )
            // InternalPaymentPlatform.g:1003:2: rule__Adapter__Group__6__Impl rule__Adapter__Group__7
            {
            pushFollow(FOLLOW_18);
            rule__Adapter__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Adapter__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__6"


    // $ANTLR start "rule__Adapter__Group__6__Impl"
    // InternalPaymentPlatform.g:1010:1: rule__Adapter__Group__6__Impl : ( ( rule__Adapter__RepositoryAssignment_6 ) ) ;
    public final void rule__Adapter__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1014:1: ( ( ( rule__Adapter__RepositoryAssignment_6 ) ) )
            // InternalPaymentPlatform.g:1015:1: ( ( rule__Adapter__RepositoryAssignment_6 ) )
            {
            // InternalPaymentPlatform.g:1015:1: ( ( rule__Adapter__RepositoryAssignment_6 ) )
            // InternalPaymentPlatform.g:1016:2: ( rule__Adapter__RepositoryAssignment_6 )
            {
             before(grammarAccess.getAdapterAccess().getRepositoryAssignment_6()); 
            // InternalPaymentPlatform.g:1017:2: ( rule__Adapter__RepositoryAssignment_6 )
            // InternalPaymentPlatform.g:1017:3: rule__Adapter__RepositoryAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Adapter__RepositoryAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getAdapterAccess().getRepositoryAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__6__Impl"


    // $ANTLR start "rule__Adapter__Group__7"
    // InternalPaymentPlatform.g:1025:1: rule__Adapter__Group__7 : rule__Adapter__Group__7__Impl ;
    public final void rule__Adapter__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1029:1: ( rule__Adapter__Group__7__Impl )
            // InternalPaymentPlatform.g:1030:2: rule__Adapter__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Adapter__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__7"


    // $ANTLR start "rule__Adapter__Group__7__Impl"
    // InternalPaymentPlatform.g:1036:1: rule__Adapter__Group__7__Impl : ( RULE_RIGHT_BRACKET ) ;
    public final void rule__Adapter__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1040:1: ( ( RULE_RIGHT_BRACKET ) )
            // InternalPaymentPlatform.g:1041:1: ( RULE_RIGHT_BRACKET )
            {
            // InternalPaymentPlatform.g:1041:1: ( RULE_RIGHT_BRACKET )
            // InternalPaymentPlatform.g:1042:2: RULE_RIGHT_BRACKET
            {
             before(grammarAccess.getAdapterAccess().getRIGHT_BRACKETTerminalRuleCall_7()); 
            match(input,RULE_RIGHT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getAdapterAccess().getRIGHT_BRACKETTerminalRuleCall_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__Group__7__Impl"


    // $ANTLR start "rule__Repository__Group__0"
    // InternalPaymentPlatform.g:1052:1: rule__Repository__Group__0 : rule__Repository__Group__0__Impl rule__Repository__Group__1 ;
    public final void rule__Repository__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1056:1: ( rule__Repository__Group__0__Impl rule__Repository__Group__1 )
            // InternalPaymentPlatform.g:1057:2: rule__Repository__Group__0__Impl rule__Repository__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Repository__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repository__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__0"


    // $ANTLR start "rule__Repository__Group__0__Impl"
    // InternalPaymentPlatform.g:1064:1: rule__Repository__Group__0__Impl : ( RULE_LEFT_BRACKET ) ;
    public final void rule__Repository__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1068:1: ( ( RULE_LEFT_BRACKET ) )
            // InternalPaymentPlatform.g:1069:1: ( RULE_LEFT_BRACKET )
            {
            // InternalPaymentPlatform.g:1069:1: ( RULE_LEFT_BRACKET )
            // InternalPaymentPlatform.g:1070:2: RULE_LEFT_BRACKET
            {
             before(grammarAccess.getRepositoryAccess().getLEFT_BRACKETTerminalRuleCall_0()); 
            match(input,RULE_LEFT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getLEFT_BRACKETTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__0__Impl"


    // $ANTLR start "rule__Repository__Group__1"
    // InternalPaymentPlatform.g:1079:1: rule__Repository__Group__1 : rule__Repository__Group__1__Impl rule__Repository__Group__2 ;
    public final void rule__Repository__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1083:1: ( rule__Repository__Group__1__Impl rule__Repository__Group__2 )
            // InternalPaymentPlatform.g:1084:2: rule__Repository__Group__1__Impl rule__Repository__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Repository__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repository__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__1"


    // $ANTLR start "rule__Repository__Group__1__Impl"
    // InternalPaymentPlatform.g:1091:1: rule__Repository__Group__1__Impl : ( 'url' ) ;
    public final void rule__Repository__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1095:1: ( ( 'url' ) )
            // InternalPaymentPlatform.g:1096:1: ( 'url' )
            {
            // InternalPaymentPlatform.g:1096:1: ( 'url' )
            // InternalPaymentPlatform.g:1097:2: 'url'
            {
             before(grammarAccess.getRepositoryAccess().getUrlKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getUrlKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__1__Impl"


    // $ANTLR start "rule__Repository__Group__2"
    // InternalPaymentPlatform.g:1106:1: rule__Repository__Group__2 : rule__Repository__Group__2__Impl rule__Repository__Group__3 ;
    public final void rule__Repository__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1110:1: ( rule__Repository__Group__2__Impl rule__Repository__Group__3 )
            // InternalPaymentPlatform.g:1111:2: rule__Repository__Group__2__Impl rule__Repository__Group__3
            {
            pushFollow(FOLLOW_20);
            rule__Repository__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repository__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__2"


    // $ANTLR start "rule__Repository__Group__2__Impl"
    // InternalPaymentPlatform.g:1118:1: rule__Repository__Group__2__Impl : ( ( rule__Repository__UrlAssignment_2 ) ) ;
    public final void rule__Repository__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1122:1: ( ( ( rule__Repository__UrlAssignment_2 ) ) )
            // InternalPaymentPlatform.g:1123:1: ( ( rule__Repository__UrlAssignment_2 ) )
            {
            // InternalPaymentPlatform.g:1123:1: ( ( rule__Repository__UrlAssignment_2 ) )
            // InternalPaymentPlatform.g:1124:2: ( rule__Repository__UrlAssignment_2 )
            {
             before(grammarAccess.getRepositoryAccess().getUrlAssignment_2()); 
            // InternalPaymentPlatform.g:1125:2: ( rule__Repository__UrlAssignment_2 )
            // InternalPaymentPlatform.g:1125:3: rule__Repository__UrlAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Repository__UrlAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRepositoryAccess().getUrlAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__2__Impl"


    // $ANTLR start "rule__Repository__Group__3"
    // InternalPaymentPlatform.g:1133:1: rule__Repository__Group__3 : rule__Repository__Group__3__Impl rule__Repository__Group__4 ;
    public final void rule__Repository__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1137:1: ( rule__Repository__Group__3__Impl rule__Repository__Group__4 )
            // InternalPaymentPlatform.g:1138:2: rule__Repository__Group__3__Impl rule__Repository__Group__4
            {
            pushFollow(FOLLOW_20);
            rule__Repository__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repository__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__3"


    // $ANTLR start "rule__Repository__Group__3__Impl"
    // InternalPaymentPlatform.g:1145:1: rule__Repository__Group__3__Impl : ( ( rule__Repository__Group_3__0 )? ) ;
    public final void rule__Repository__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1149:1: ( ( ( rule__Repository__Group_3__0 )? ) )
            // InternalPaymentPlatform.g:1150:1: ( ( rule__Repository__Group_3__0 )? )
            {
            // InternalPaymentPlatform.g:1150:1: ( ( rule__Repository__Group_3__0 )? )
            // InternalPaymentPlatform.g:1151:2: ( rule__Repository__Group_3__0 )?
            {
             before(grammarAccess.getRepositoryAccess().getGroup_3()); 
            // InternalPaymentPlatform.g:1152:2: ( rule__Repository__Group_3__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==32) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalPaymentPlatform.g:1152:3: rule__Repository__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Repository__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRepositoryAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__3__Impl"


    // $ANTLR start "rule__Repository__Group__4"
    // InternalPaymentPlatform.g:1160:1: rule__Repository__Group__4 : rule__Repository__Group__4__Impl ;
    public final void rule__Repository__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1164:1: ( rule__Repository__Group__4__Impl )
            // InternalPaymentPlatform.g:1165:2: rule__Repository__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Repository__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__4"


    // $ANTLR start "rule__Repository__Group__4__Impl"
    // InternalPaymentPlatform.g:1171:1: rule__Repository__Group__4__Impl : ( RULE_RIGHT_BRACKET ) ;
    public final void rule__Repository__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1175:1: ( ( RULE_RIGHT_BRACKET ) )
            // InternalPaymentPlatform.g:1176:1: ( RULE_RIGHT_BRACKET )
            {
            // InternalPaymentPlatform.g:1176:1: ( RULE_RIGHT_BRACKET )
            // InternalPaymentPlatform.g:1177:2: RULE_RIGHT_BRACKET
            {
             before(grammarAccess.getRepositoryAccess().getRIGHT_BRACKETTerminalRuleCall_4()); 
            match(input,RULE_RIGHT_BRACKET,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getRIGHT_BRACKETTerminalRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group__4__Impl"


    // $ANTLR start "rule__Repository__Group_3__0"
    // InternalPaymentPlatform.g:1187:1: rule__Repository__Group_3__0 : rule__Repository__Group_3__0__Impl rule__Repository__Group_3__1 ;
    public final void rule__Repository__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1191:1: ( rule__Repository__Group_3__0__Impl rule__Repository__Group_3__1 )
            // InternalPaymentPlatform.g:1192:2: rule__Repository__Group_3__0__Impl rule__Repository__Group_3__1
            {
            pushFollow(FOLLOW_4);
            rule__Repository__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Repository__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group_3__0"


    // $ANTLR start "rule__Repository__Group_3__0__Impl"
    // InternalPaymentPlatform.g:1199:1: rule__Repository__Group_3__0__Impl : ( 'branch' ) ;
    public final void rule__Repository__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1203:1: ( ( 'branch' ) )
            // InternalPaymentPlatform.g:1204:1: ( 'branch' )
            {
            // InternalPaymentPlatform.g:1204:1: ( 'branch' )
            // InternalPaymentPlatform.g:1205:2: 'branch'
            {
             before(grammarAccess.getRepositoryAccess().getBranchKeyword_3_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getRepositoryAccess().getBranchKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group_3__0__Impl"


    // $ANTLR start "rule__Repository__Group_3__1"
    // InternalPaymentPlatform.g:1214:1: rule__Repository__Group_3__1 : rule__Repository__Group_3__1__Impl ;
    public final void rule__Repository__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1218:1: ( rule__Repository__Group_3__1__Impl )
            // InternalPaymentPlatform.g:1219:2: rule__Repository__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Repository__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group_3__1"


    // $ANTLR start "rule__Repository__Group_3__1__Impl"
    // InternalPaymentPlatform.g:1225:1: rule__Repository__Group_3__1__Impl : ( ( rule__Repository__BranchAssignment_3_1 ) ) ;
    public final void rule__Repository__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1229:1: ( ( ( rule__Repository__BranchAssignment_3_1 ) ) )
            // InternalPaymentPlatform.g:1230:1: ( ( rule__Repository__BranchAssignment_3_1 ) )
            {
            // InternalPaymentPlatform.g:1230:1: ( ( rule__Repository__BranchAssignment_3_1 ) )
            // InternalPaymentPlatform.g:1231:2: ( rule__Repository__BranchAssignment_3_1 )
            {
             before(grammarAccess.getRepositoryAccess().getBranchAssignment_3_1()); 
            // InternalPaymentPlatform.g:1232:2: ( rule__Repository__BranchAssignment_3_1 )
            // InternalPaymentPlatform.g:1232:3: rule__Repository__BranchAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Repository__BranchAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getRepositoryAccess().getBranchAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__Group_3__1__Impl"


    // $ANTLR start "rule__RestPaymentNetwork__Group__0"
    // InternalPaymentPlatform.g:1241:1: rule__RestPaymentNetwork__Group__0 : rule__RestPaymentNetwork__Group__0__Impl rule__RestPaymentNetwork__Group__1 ;
    public final void rule__RestPaymentNetwork__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1245:1: ( rule__RestPaymentNetwork__Group__0__Impl rule__RestPaymentNetwork__Group__1 )
            // InternalPaymentPlatform.g:1246:2: rule__RestPaymentNetwork__Group__0__Impl rule__RestPaymentNetwork__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__RestPaymentNetwork__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RestPaymentNetwork__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestPaymentNetwork__Group__0"


    // $ANTLR start "rule__RestPaymentNetwork__Group__0__Impl"
    // InternalPaymentPlatform.g:1253:1: rule__RestPaymentNetwork__Group__0__Impl : ( () ) ;
    public final void rule__RestPaymentNetwork__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1257:1: ( ( () ) )
            // InternalPaymentPlatform.g:1258:1: ( () )
            {
            // InternalPaymentPlatform.g:1258:1: ( () )
            // InternalPaymentPlatform.g:1259:2: ()
            {
             before(grammarAccess.getRestPaymentNetworkAccess().getRestPaymentNetworkAction_0()); 
            // InternalPaymentPlatform.g:1260:2: ()
            // InternalPaymentPlatform.g:1260:3: 
            {
            }

             after(grammarAccess.getRestPaymentNetworkAccess().getRestPaymentNetworkAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestPaymentNetwork__Group__0__Impl"


    // $ANTLR start "rule__RestPaymentNetwork__Group__1"
    // InternalPaymentPlatform.g:1268:1: rule__RestPaymentNetwork__Group__1 : rule__RestPaymentNetwork__Group__1__Impl rule__RestPaymentNetwork__Group__2 ;
    public final void rule__RestPaymentNetwork__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1272:1: ( rule__RestPaymentNetwork__Group__1__Impl rule__RestPaymentNetwork__Group__2 )
            // InternalPaymentPlatform.g:1273:2: rule__RestPaymentNetwork__Group__1__Impl rule__RestPaymentNetwork__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__RestPaymentNetwork__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RestPaymentNetwork__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestPaymentNetwork__Group__1"


    // $ANTLR start "rule__RestPaymentNetwork__Group__1__Impl"
    // InternalPaymentPlatform.g:1280:1: rule__RestPaymentNetwork__Group__1__Impl : ( 'RestPaymentNetwork' ) ;
    public final void rule__RestPaymentNetwork__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1284:1: ( ( 'RestPaymentNetwork' ) )
            // InternalPaymentPlatform.g:1285:1: ( 'RestPaymentNetwork' )
            {
            // InternalPaymentPlatform.g:1285:1: ( 'RestPaymentNetwork' )
            // InternalPaymentPlatform.g:1286:2: 'RestPaymentNetwork'
            {
             before(grammarAccess.getRestPaymentNetworkAccess().getRestPaymentNetworkKeyword_1()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getRestPaymentNetworkAccess().getRestPaymentNetworkKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestPaymentNetwork__Group__1__Impl"


    // $ANTLR start "rule__RestPaymentNetwork__Group__2"
    // InternalPaymentPlatform.g:1295:1: rule__RestPaymentNetwork__Group__2 : rule__RestPaymentNetwork__Group__2__Impl ;
    public final void rule__RestPaymentNetwork__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1299:1: ( rule__RestPaymentNetwork__Group__2__Impl )
            // InternalPaymentPlatform.g:1300:2: rule__RestPaymentNetwork__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RestPaymentNetwork__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestPaymentNetwork__Group__2"


    // $ANTLR start "rule__RestPaymentNetwork__Group__2__Impl"
    // InternalPaymentPlatform.g:1306:1: rule__RestPaymentNetwork__Group__2__Impl : ( ( rule__RestPaymentNetwork__NameAssignment_2 ) ) ;
    public final void rule__RestPaymentNetwork__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1310:1: ( ( ( rule__RestPaymentNetwork__NameAssignment_2 ) ) )
            // InternalPaymentPlatform.g:1311:1: ( ( rule__RestPaymentNetwork__NameAssignment_2 ) )
            {
            // InternalPaymentPlatform.g:1311:1: ( ( rule__RestPaymentNetwork__NameAssignment_2 ) )
            // InternalPaymentPlatform.g:1312:2: ( rule__RestPaymentNetwork__NameAssignment_2 )
            {
             before(grammarAccess.getRestPaymentNetworkAccess().getNameAssignment_2()); 
            // InternalPaymentPlatform.g:1313:2: ( rule__RestPaymentNetwork__NameAssignment_2 )
            // InternalPaymentPlatform.g:1313:3: rule__RestPaymentNetwork__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__RestPaymentNetwork__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRestPaymentNetworkAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestPaymentNetwork__Group__2__Impl"


    // $ANTLR start "rule__SOAPPaymentNetwork__Group__0"
    // InternalPaymentPlatform.g:1322:1: rule__SOAPPaymentNetwork__Group__0 : rule__SOAPPaymentNetwork__Group__0__Impl rule__SOAPPaymentNetwork__Group__1 ;
    public final void rule__SOAPPaymentNetwork__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1326:1: ( rule__SOAPPaymentNetwork__Group__0__Impl rule__SOAPPaymentNetwork__Group__1 )
            // InternalPaymentPlatform.g:1327:2: rule__SOAPPaymentNetwork__Group__0__Impl rule__SOAPPaymentNetwork__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__SOAPPaymentNetwork__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOAPPaymentNetwork__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOAPPaymentNetwork__Group__0"


    // $ANTLR start "rule__SOAPPaymentNetwork__Group__0__Impl"
    // InternalPaymentPlatform.g:1334:1: rule__SOAPPaymentNetwork__Group__0__Impl : ( () ) ;
    public final void rule__SOAPPaymentNetwork__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1338:1: ( ( () ) )
            // InternalPaymentPlatform.g:1339:1: ( () )
            {
            // InternalPaymentPlatform.g:1339:1: ( () )
            // InternalPaymentPlatform.g:1340:2: ()
            {
             before(grammarAccess.getSOAPPaymentNetworkAccess().getSOAPPaymentNetworkAction_0()); 
            // InternalPaymentPlatform.g:1341:2: ()
            // InternalPaymentPlatform.g:1341:3: 
            {
            }

             after(grammarAccess.getSOAPPaymentNetworkAccess().getSOAPPaymentNetworkAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOAPPaymentNetwork__Group__0__Impl"


    // $ANTLR start "rule__SOAPPaymentNetwork__Group__1"
    // InternalPaymentPlatform.g:1349:1: rule__SOAPPaymentNetwork__Group__1 : rule__SOAPPaymentNetwork__Group__1__Impl rule__SOAPPaymentNetwork__Group__2 ;
    public final void rule__SOAPPaymentNetwork__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1353:1: ( rule__SOAPPaymentNetwork__Group__1__Impl rule__SOAPPaymentNetwork__Group__2 )
            // InternalPaymentPlatform.g:1354:2: rule__SOAPPaymentNetwork__Group__1__Impl rule__SOAPPaymentNetwork__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__SOAPPaymentNetwork__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SOAPPaymentNetwork__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOAPPaymentNetwork__Group__1"


    // $ANTLR start "rule__SOAPPaymentNetwork__Group__1__Impl"
    // InternalPaymentPlatform.g:1361:1: rule__SOAPPaymentNetwork__Group__1__Impl : ( 'SOAPPaymentNetwork' ) ;
    public final void rule__SOAPPaymentNetwork__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1365:1: ( ( 'SOAPPaymentNetwork' ) )
            // InternalPaymentPlatform.g:1366:1: ( 'SOAPPaymentNetwork' )
            {
            // InternalPaymentPlatform.g:1366:1: ( 'SOAPPaymentNetwork' )
            // InternalPaymentPlatform.g:1367:2: 'SOAPPaymentNetwork'
            {
             before(grammarAccess.getSOAPPaymentNetworkAccess().getSOAPPaymentNetworkKeyword_1()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getSOAPPaymentNetworkAccess().getSOAPPaymentNetworkKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOAPPaymentNetwork__Group__1__Impl"


    // $ANTLR start "rule__SOAPPaymentNetwork__Group__2"
    // InternalPaymentPlatform.g:1376:1: rule__SOAPPaymentNetwork__Group__2 : rule__SOAPPaymentNetwork__Group__2__Impl ;
    public final void rule__SOAPPaymentNetwork__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1380:1: ( rule__SOAPPaymentNetwork__Group__2__Impl )
            // InternalPaymentPlatform.g:1381:2: rule__SOAPPaymentNetwork__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SOAPPaymentNetwork__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOAPPaymentNetwork__Group__2"


    // $ANTLR start "rule__SOAPPaymentNetwork__Group__2__Impl"
    // InternalPaymentPlatform.g:1387:1: rule__SOAPPaymentNetwork__Group__2__Impl : ( ( rule__SOAPPaymentNetwork__NameAssignment_2 ) ) ;
    public final void rule__SOAPPaymentNetwork__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1391:1: ( ( ( rule__SOAPPaymentNetwork__NameAssignment_2 ) ) )
            // InternalPaymentPlatform.g:1392:1: ( ( rule__SOAPPaymentNetwork__NameAssignment_2 ) )
            {
            // InternalPaymentPlatform.g:1392:1: ( ( rule__SOAPPaymentNetwork__NameAssignment_2 ) )
            // InternalPaymentPlatform.g:1393:2: ( rule__SOAPPaymentNetwork__NameAssignment_2 )
            {
             before(grammarAccess.getSOAPPaymentNetworkAccess().getNameAssignment_2()); 
            // InternalPaymentPlatform.g:1394:2: ( rule__SOAPPaymentNetwork__NameAssignment_2 )
            // InternalPaymentPlatform.g:1394:3: rule__SOAPPaymentNetwork__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SOAPPaymentNetwork__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSOAPPaymentNetworkAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOAPPaymentNetwork__Group__2__Impl"


    // $ANTLR start "rule__PaymentPlaform__NameAssignment_2"
    // InternalPaymentPlatform.g:1403:1: rule__PaymentPlaform__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__PaymentPlaform__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1407:1: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:1408:2: ( ruleEString )
            {
            // InternalPaymentPlatform.g:1408:2: ( ruleEString )
            // InternalPaymentPlatform.g:1409:3: ruleEString
            {
             before(grammarAccess.getPaymentPlaformAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getPaymentPlaformAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__NameAssignment_2"


    // $ANTLR start "rule__PaymentPlaform__AdapterAssignment_7"
    // InternalPaymentPlatform.g:1418:1: rule__PaymentPlaform__AdapterAssignment_7 : ( ruleAdapter ) ;
    public final void rule__PaymentPlaform__AdapterAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1422:1: ( ( ruleAdapter ) )
            // InternalPaymentPlatform.g:1423:2: ( ruleAdapter )
            {
            // InternalPaymentPlatform.g:1423:2: ( ruleAdapter )
            // InternalPaymentPlatform.g:1424:3: ruleAdapter
            {
             before(grammarAccess.getPaymentPlaformAccess().getAdapterAdapterParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleAdapter();

            state._fsp--;

             after(grammarAccess.getPaymentPlaformAccess().getAdapterAdapterParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__AdapterAssignment_7"


    // $ANTLR start "rule__PaymentPlaform__AdapterAssignment_8_1"
    // InternalPaymentPlatform.g:1433:1: rule__PaymentPlaform__AdapterAssignment_8_1 : ( ruleAdapter ) ;
    public final void rule__PaymentPlaform__AdapterAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1437:1: ( ( ruleAdapter ) )
            // InternalPaymentPlatform.g:1438:2: ( ruleAdapter )
            {
            // InternalPaymentPlatform.g:1438:2: ( ruleAdapter )
            // InternalPaymentPlatform.g:1439:3: ruleAdapter
            {
             before(grammarAccess.getPaymentPlaformAccess().getAdapterAdapterParserRuleCall_8_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAdapter();

            state._fsp--;

             after(grammarAccess.getPaymentPlaformAccess().getAdapterAdapterParserRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__AdapterAssignment_8_1"


    // $ANTLR start "rule__PaymentPlaform__PaymentnetworkAssignment_14"
    // InternalPaymentPlatform.g:1448:1: rule__PaymentPlaform__PaymentnetworkAssignment_14 : ( rulePaymentNetwork ) ;
    public final void rule__PaymentPlaform__PaymentnetworkAssignment_14() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1452:1: ( ( rulePaymentNetwork ) )
            // InternalPaymentPlatform.g:1453:2: ( rulePaymentNetwork )
            {
            // InternalPaymentPlatform.g:1453:2: ( rulePaymentNetwork )
            // InternalPaymentPlatform.g:1454:3: rulePaymentNetwork
            {
             before(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkPaymentNetworkParserRuleCall_14_0()); 
            pushFollow(FOLLOW_2);
            rulePaymentNetwork();

            state._fsp--;

             after(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkPaymentNetworkParserRuleCall_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__PaymentnetworkAssignment_14"


    // $ANTLR start "rule__PaymentPlaform__PaymentnetworkAssignment_15_1"
    // InternalPaymentPlatform.g:1463:1: rule__PaymentPlaform__PaymentnetworkAssignment_15_1 : ( rulePaymentNetwork ) ;
    public final void rule__PaymentPlaform__PaymentnetworkAssignment_15_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1467:1: ( ( rulePaymentNetwork ) )
            // InternalPaymentPlatform.g:1468:2: ( rulePaymentNetwork )
            {
            // InternalPaymentPlatform.g:1468:2: ( rulePaymentNetwork )
            // InternalPaymentPlatform.g:1469:3: rulePaymentNetwork
            {
             before(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkPaymentNetworkParserRuleCall_15_1_0()); 
            pushFollow(FOLLOW_2);
            rulePaymentNetwork();

            state._fsp--;

             after(grammarAccess.getPaymentPlaformAccess().getPaymentnetworkPaymentNetworkParserRuleCall_15_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PaymentPlaform__PaymentnetworkAssignment_15_1"


    // $ANTLR start "rule__Adapter__NameAssignment_0"
    // InternalPaymentPlatform.g:1478:1: rule__Adapter__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__Adapter__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1482:1: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:1483:2: ( ruleEString )
            {
            // InternalPaymentPlatform.g:1483:2: ( ruleEString )
            // InternalPaymentPlatform.g:1484:3: ruleEString
            {
             before(grammarAccess.getAdapterAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAdapterAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__NameAssignment_0"


    // $ANTLR start "rule__Adapter__PaymentnetworkAssignment_3"
    // InternalPaymentPlatform.g:1493:1: rule__Adapter__PaymentnetworkAssignment_3 : ( ( ruleEString ) ) ;
    public final void rule__Adapter__PaymentnetworkAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1497:1: ( ( ( ruleEString ) ) )
            // InternalPaymentPlatform.g:1498:2: ( ( ruleEString ) )
            {
            // InternalPaymentPlatform.g:1498:2: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:1499:3: ( ruleEString )
            {
             before(grammarAccess.getAdapterAccess().getPaymentnetworkPaymentNetworkCrossReference_3_0()); 
            // InternalPaymentPlatform.g:1500:3: ( ruleEString )
            // InternalPaymentPlatform.g:1501:4: ruleEString
            {
             before(grammarAccess.getAdapterAccess().getPaymentnetworkPaymentNetworkEStringParserRuleCall_3_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAdapterAccess().getPaymentnetworkPaymentNetworkEStringParserRuleCall_3_0_1()); 

            }

             after(grammarAccess.getAdapterAccess().getPaymentnetworkPaymentNetworkCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__PaymentnetworkAssignment_3"


    // $ANTLR start "rule__Adapter__RepositoryAssignment_6"
    // InternalPaymentPlatform.g:1512:1: rule__Adapter__RepositoryAssignment_6 : ( ruleRepository ) ;
    public final void rule__Adapter__RepositoryAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1516:1: ( ( ruleRepository ) )
            // InternalPaymentPlatform.g:1517:2: ( ruleRepository )
            {
            // InternalPaymentPlatform.g:1517:2: ( ruleRepository )
            // InternalPaymentPlatform.g:1518:3: ruleRepository
            {
             before(grammarAccess.getAdapterAccess().getRepositoryRepositoryParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleRepository();

            state._fsp--;

             after(grammarAccess.getAdapterAccess().getRepositoryRepositoryParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Adapter__RepositoryAssignment_6"


    // $ANTLR start "rule__Repository__UrlAssignment_2"
    // InternalPaymentPlatform.g:1527:1: rule__Repository__UrlAssignment_2 : ( ruleEString ) ;
    public final void rule__Repository__UrlAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1531:1: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:1532:2: ( ruleEString )
            {
            // InternalPaymentPlatform.g:1532:2: ( ruleEString )
            // InternalPaymentPlatform.g:1533:3: ruleEString
            {
             before(grammarAccess.getRepositoryAccess().getUrlEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRepositoryAccess().getUrlEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__UrlAssignment_2"


    // $ANTLR start "rule__Repository__BranchAssignment_3_1"
    // InternalPaymentPlatform.g:1542:1: rule__Repository__BranchAssignment_3_1 : ( ruleEString ) ;
    public final void rule__Repository__BranchAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1546:1: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:1547:2: ( ruleEString )
            {
            // InternalPaymentPlatform.g:1547:2: ( ruleEString )
            // InternalPaymentPlatform.g:1548:3: ruleEString
            {
             before(grammarAccess.getRepositoryAccess().getBranchEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRepositoryAccess().getBranchEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Repository__BranchAssignment_3_1"


    // $ANTLR start "rule__RestPaymentNetwork__NameAssignment_2"
    // InternalPaymentPlatform.g:1557:1: rule__RestPaymentNetwork__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__RestPaymentNetwork__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1561:1: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:1562:2: ( ruleEString )
            {
            // InternalPaymentPlatform.g:1562:2: ( ruleEString )
            // InternalPaymentPlatform.g:1563:3: ruleEString
            {
             before(grammarAccess.getRestPaymentNetworkAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getRestPaymentNetworkAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RestPaymentNetwork__NameAssignment_2"


    // $ANTLR start "rule__SOAPPaymentNetwork__NameAssignment_2"
    // InternalPaymentPlatform.g:1572:1: rule__SOAPPaymentNetwork__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__SOAPPaymentNetwork__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalPaymentPlatform.g:1576:1: ( ( ruleEString ) )
            // InternalPaymentPlatform.g:1577:2: ( ruleEString )
            {
            // InternalPaymentPlatform.g:1577:2: ( ruleEString )
            // InternalPaymentPlatform.g:1578:3: ruleEString
            {
             before(grammarAccess.getSOAPPaymentNetworkAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getSOAPPaymentNetworkAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SOAPPaymentNetwork__NameAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000300L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000400L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000100000800L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000200000000L});

}