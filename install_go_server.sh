#!/bin/bash

go get github.com/docker/go-units
go get github.com/go-openapi/validate
go install ./cmd/rede-server/
rede-server --port 3000/