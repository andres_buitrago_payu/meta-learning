#!/bin/zsh 

echo "enter the repo name:"
read repo
echo "enter the branch name:"
read branch
echo "cloning the branch $branch"
pwd=$(pwd)
activemq_read=localhost:8161
activemq_write=localhost
queue=aa.authorization.request.redecard

cd ~/
git clone --single-branch --branch $branch https://bitbucket.org/pagosonline/$repo.git

cd ~/$repo
mvn spring-boot:run -Drun.arguments="--houston.jms.activemq.brokerUrl=$activemq_read" &
sleep 100
cd $pwd/nodejs-activemq
npm install
echo "module.exports = {
    host: \"$activemq_write\",
    queue: \"$queue\",
    topic: \"/topic/topic1\",
}" > "settings.js"
node app.js 